package com.team20.unicorn

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.team20.unicorn.databinding.FragmentPostBinding
import com.team20.unicorn.helper.ConnectionHelper
import com.team20.unicorn.viewModel.PostViewModel
import com.team20.unicorn.viewModel.PostViewModelFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PostFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PostFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var viewModel: PostViewModel
    private lateinit var viewModelFactory: PostViewModelFactory
    private lateinit var binding : FragmentPostBinding
    private lateinit var bttnUpload: Button
    private lateinit var cancelBttn: Button
    private lateinit var txtContent: TextView
    private lateinit var bttnImg: ImageButton
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate<FragmentPostBinding>(inflater,R.layout.fragment_post,container,false)
        var id = ""
        val activity = activity as MainActivity
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null) {
            id = sharedPref.getString("userId","").toString();
        }
        viewModelFactory = PostViewModelFactory(id)

        viewModel = ViewModelProvider(this,viewModelFactory)
            .get(PostViewModel::class.java)
        txtContent = binding.editPostText
        bttnUpload = binding.bttnUpload
        bttnImg = binding.imgPost
        cancelBttn = binding.bttnCancel
        cancelBttn.setOnClickListener{
            requireView().findNavController()
                .navigate(R.id.action_postFragment_to_mainFragment)
        }
        bttnImg.setOnClickListener{
            if(askForGalleryPermissions()){
                selectImageFromGallery()
            }
        }
        bttnUpload.setOnClickListener{
            if (txtContent.text.toString().isNotEmpty()) {
                viewModel.uploadPost(txtContent.text.toString())
                requireView().findNavController()
                    .navigate(R.id.action_postFragment_to_mainFragment)
            } else {
                showRequiredFieldsDialog()
            }
            if(!connectionHelper.checkForInternet(requireContext())) {
                val alertDialog = androidx.appcompat.app.AlertDialog.Builder(requireContext())
                alertDialog.apply {
                    setTitle("Internet connection")
                    setMessage("You don't have internet connection. You new post will be upload when you have internet connection again")
                    setNeutralButton("Ok",
                        DialogInterface.OnClickListener { dialog, id ->

                        })
                }.create().show()
            }

        }
        return binding.root
    }
    fun selectImageFromGallery(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, "image/jpeg")
        startActivityForResult(intent, 1)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 1){
            val bitmap = MediaStore.Images.Media.getBitmap((activity as MainActivity).contentResolver, data?.data)
            val prueba = data?.data?.toString()?:"No"
            val newBmp = viewModel.resizeBitmap(bitmap,300)
            bttnImg.setImageBitmap(newBmp)
            viewModel.postUri = prueba?.toUri()!!
        }
    }

    private fun isGalleryPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(activity as MainActivity,
            Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }
    private fun showPermissionDeniedDialog() {

        AlertDialog.Builder(activity)
            .setTitle("Permission Denied")
            .setMessage("Permission to access Gallery is denied. Please allow permissions from App Settings to select an image from your phone's gallery.")
            .setPositiveButton("App Settings",
                DialogInterface.OnClickListener { dialogInterface, i ->
                    // send to app settings if permission is denied permanently
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package", activity?.packageName, null)
                    intent.data = uri
                    startActivity(intent)
                })
            .setNegativeButton("Cancel",null)
            .show()
    }
    private fun showRequiredFieldsDialog() {

        AlertDialog.Builder(activity)
            .setTitle("Post failed")
            .setMessage("You must write a text for your post!. Images are optional.")
            .setPositiveButton("Ok", DialogInterface.OnClickListener { dialogInterface, i ->
                dialogInterface.dismiss()
            }).show()

    }
    private fun askForGalleryPermissions(): Boolean {
        if (!isGalleryPermissionsAllowed()) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showPermissionDeniedDialog()
            } else {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),1)
            }
            return false
        }
        return true
    }
    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<String>,grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImageFromGallery()

                } else {
                    showPermissionDeniedDialog()

                }
                return
            }
        }
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PostFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PostFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

}