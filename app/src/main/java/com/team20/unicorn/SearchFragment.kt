package com.team20.unicorn

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.team20.unicorn.databinding.FragmentSearchBinding
import com.team20.unicorn.helper.CustomGlideListener
import com.team20.unicorn.viewModel.SearchViewModel
import com.team20.unicorn.viewModel.SearchViewModelFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SearchFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentSearchBinding
    private lateinit var viewModel: SearchViewModel
    private lateinit var viewModelFactory: SearchViewModelFactory


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_search,container,false)

        binding.profileButton.setOnClickListener {
            var see = parentFragment
            when(see){
                is HomeFragment -> see.swapToProfileFragment()
            }
        }
        binding.searchButton.setOnClickListener {
            var see = parentFragment
            when(see){
                is HomeFragment -> see.swapToSearchFragment()
            }
        }

        var id = ""
        val activity = activity as MainActivity
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null) {
            id = sharedPref.getString("userId","").toString();
        }

        viewModelFactory = SearchViewModelFactory(id)

        viewModel = ViewModelProvider(this,viewModelFactory)
            .get(SearchViewModel::class.java)

        viewModel.profilePic.observe(viewLifecycleOwner, Observer { newPic ->
            loadProfilePic(binding.profileButton, newPic,"users/${viewModel.userID}/profile.jpeg")
        })



        binding.searchButton.setOnClickListener {
            var see = parentFragment
            when(see){
                is HomeFragment -> see.swapToSearchFragment()
            }
        }


        return binding.root
    }


    fun loadProfilePic(imageView: ImageView, use: Pair<String,String>, url: String){
        var myTrace = Firebase.performance.newTrace("get_image")
        myTrace.start()
        Glide.with(this)
            .load(use.first)
            .listener(CustomGlideListener(myTrace,url,true))
            .error(
                Glide.with(this)
                    .load(use.second.toUri())
                    .listener(CustomGlideListener(null,url,false))
                    .apply(
                        RequestOptions()
                            .placeholder(R.drawable.ic_baseline_add_photo_alternate_24)
                            .error(R.drawable.ic_baseline_warning_24)
                    )
            )
            .centerInside()
            .into(imageView)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SearchFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SearchFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}