package com.team20.unicorn

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.team20.unicorn.databinding.FragmentGenericDashboardBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [GenericDashboardFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GenericDashboardFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding : FragmentGenericDashboardBinding
    private var actual : String = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentGenericDashboardBinding>(inflater,R.layout.fragment_generic_dashboard,container,false)
        val activity = activity as MainActivity

        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if (sharedPref != null) {
            with(sharedPref.edit()) {
                remove("tempId")
                apply()
            }
        }

        actual = "HOME"

        binding.bottomNavigationView.setOnNavigationItemSelectedListener {
            replaceUpperFragment(it)
        }

        return binding.root
    }

    private fun replaceUpperFragment(it : MenuItem ): Boolean {
        var manager = childFragmentManager
        var see = manager.fragments[0]
        var controller = see.requireView().findNavController()

        when(it.itemId){
            R.id.Pages ->{
                when(actual){
                    "HOME" -> controller.navigate(R.id.action_homeFragment_to_pagesViewsFragment)
                    "WEEKLY" -> controller.navigate(R.id.action_weeklyHotFragment_to_pagesViewsFragment)
                    "INDUSTRIES" -> controller.navigate(R.id.action_trendsIndustriesFragment_to_pagesViewsFragment)
                    "CALENDAR" -> controller.navigate(R.id.action_calendarFragment_to_pagesViewsFragment)
                    else -> {}
                }
                actual = "PAGES"
            }
            R.id.Home -> {
                when(actual){
                    "PAGES" -> controller.navigate(R.id.action_pagesViewsFragment_to_homeFragment)
                    "WEEKLY" -> controller.navigate(R.id.action_weeklyHotFragment_to_homeFragment)
                    "INDUSTRIES" -> controller.navigate(R.id.action_trendsIndustriesFragment_to_homeFragment)
                    "CALENDAR" -> controller.navigate(R.id.action_calendarFragment_to_homeFragment)
                    else -> {}
                }
                actual = "HOME"
            }
            R.id.Weekly -> {
                when(actual){
                    "HOME" -> controller.navigate(R.id.action_homeFragment_to_weeklyHotFragment)
                    "PAGES" -> controller.navigate(R.id.action_pagesViewsFragment_to_weeklyHotFragment)
                    "INDUSTRIES" -> controller.navigate(R.id.action_trendsIndustriesFragment_to_weeklyHotFragment)
                    "CALENDAR" -> controller.navigate(R.id.action_calendarFragment_to_weeklyHotFragment)
                    else -> {}
                }
                actual = "WEEKLY"
            }
            R.id.Industries -> {
                when(actual){
                "HOME" -> controller.navigate(R.id.action_homeFragment_to_trendsIndustriesFragment)
                "WEEKLY" -> controller.navigate(R.id.action_weeklyHotFragment_to_trendsIndustriesFragment)
                "PAGES" -> controller.navigate(R.id.action_pagesViewsFragment_to_trendsIndustriesFragment)
                "CALENDAR" -> controller.navigate(R.id.action_calendarFragment_to_trendsIndustriesFragment)
                else -> {}
                }
                actual="INDUSTRIES"
            }
            R.id.Calendar -> {
                when(actual){
                    "HOME" -> controller.navigate(R.id.action_homeFragment_to_calendarFragment)
                    "WEEKLY" -> controller.navigate(R.id.action_weeklyHotFragment_to_calendarFragment)
                    "PAGES" -> controller.navigate(R.id.action_pagesViewsFragment_to_calendarFragment)
                    "INDUSTRIES" -> controller.navigate(R.id.action_trendsIndustriesFragment_to_calendarFragment)
                    else -> {}
                }
                actual="CALENDAR"
            }
        }

        return true
    }

    fun swapToProfileFragment(){
        requireView().findNavController().navigate(GenericDashboardFragmentDirections.actionGenericDashboardFragmentToProfileFragment())
    }

    fun swapToSearchFragment(){
        requireView().findNavController().navigate(R.id.action_genericDashboardFragment_to_searchUserFragment)
    }

    fun swapToPageFragment(){
        requireView().findNavController().navigate(GenericDashboardFragmentDirections.actionGenericDashboardFragmentToPagesFragment2())
    }

    fun swapToPagesProfile(id: String){
        requireView().findNavController().navigate(GenericDashboardFragmentDirections.actionGenericDashboardFragmentToPagesProfileFragment(id))
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment GenericDashboardFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            GenericDashboardFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}