package com.team20.unicorn
import android.app.Application
import android.content.Context

class UnicornApplication: Application() {
    init {
        instance = this
    }

    companion object {
        private var instance: UnicornApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        val context: Context = UnicornApplication.applicationContext()
    }
}