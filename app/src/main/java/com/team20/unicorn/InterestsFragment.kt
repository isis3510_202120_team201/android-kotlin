package com.team20.unicorn

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.team20.unicorn.databinding.FragmentInterestsBinding
import com.team20.unicorn.helper.InterestsAdapter
import com.team20.unicorn.viewModel.ProfileViewModel
import com.team20.unicorn.viewModel.ProfileViewModelFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [InterestsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class InterestsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var viewModel: ProfileViewModel
    private lateinit var viewModelFactory: ProfileViewModelFactory
    private lateinit var interests : MutableList<String>
    private lateinit var layout: LinearLayout
    private lateinit var interestsRecyclerView : RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentInterestsBinding>(inflater,R.layout.fragment_interests,container,false)

        val activity = activity as MainActivity
        var id = ""

        var see = parentFragment

        when(see){
            is NavHostFragment -> see = see.parentFragment
        }

        when(see){
            is ProfileFragment -> see = see as ProfileFragment
        }

        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null && sharedPref.getString("tempId","") != "") {
            id = sharedPref.getString("tempId","").toString()
        }

        if(sharedPref != null && id == "") {
            id = sharedPref.getString("userId","").toString()
        }

        viewModelFactory = ProfileViewModelFactory(id)

        if(see != null)
            viewModel = ViewModelProvider(see,viewModelFactory)
                .get(ProfileViewModel::class.java)

        interests = viewModel.interests.value!!

        interestsRecyclerView = binding.intslist
        interestsRecyclerView.layoutManager = LinearLayoutManager(activity as MainActivity)
        interestsRecyclerView.setHasFixedSize(true)
        interestsRecyclerView.adapter = InterestsAdapter(interests)
        layout = binding.interestsLayout

        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment InterestsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            InterestsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}