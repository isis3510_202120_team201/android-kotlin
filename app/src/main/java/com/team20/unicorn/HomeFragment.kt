package com.team20.unicorn

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.database.*
import com.team20.unicorn.databinding.FragmentHomeBinding
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.navigation.fragment.NavHostFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.team20.unicorn.helper.ConnectionHelper
import com.team20.unicorn.viewModel.HomeViewModel
import com.team20.unicorn.viewModel.HomeViewModelFactory
import java.io.IOException
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var postRecyclerView : RecyclerView
    private lateinit var binding : FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var viewModelFactory: HomeViewModelFactory
    lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var thisactivity: MainActivity
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)

        }
        thisactivity = activity as MainActivity
        val view=thisactivity.currentFocus
        if (view!=null)
        {
            val imm=thisactivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        val sharedPref = thisactivity.getPreferences(Context.MODE_PRIVATE)
        var id = ""
        if(sharedPref != null) {
            id = sharedPref.getString("userId","").toString();
        }

        viewModelFactory = HomeViewModelFactory(id)
        viewModel = ViewModelProvider(this,viewModelFactory)
            .get(HomeViewModel::class.java)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(thisactivity)

        if(!hasPermissions(requireContext(), *viewModel.PERMISSIONS)){
            ActivityCompat.requestPermissions(thisactivity, viewModel.PERMISSIONS,viewModel.COARSE_PERMISSION_CODE)
        }
        viewModel.informationMessage.observe(this, Observer { newInformationMessage ->
            handleInformationMessage(newInformationMessage)
        })

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
                if(location != null){
                    val geocoder  = Geocoder(requireContext(), Locale.getDefault())
                    var dirs : List<Address>? = null
                    try{
                        dirs = geocoder.getFromLocation(location.latitude,location.longitude,1)
                        viewModel.country = dirs[0].countryName
                        viewModel.getStartUpData()
                    }
                    catch (e: IOException){
                        Toast.makeText(requireContext(),"Error retrieving country code",Toast.LENGTH_LONG).show()
                    }
                }
                else{
                    Toast.makeText(requireContext(),"Please turn on your location",Toast.LENGTH_LONG).show()
                }
                // Got last known location. In some rare situations this can be null.
            }

        if(!connectionHelper.checkForInternet(requireContext()))
        {
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.apply {
                setTitle("Internet connection")
                setMessage("You don't have internet connection, we are showing you the last downloaded posts ")
                setNeutralButton("Ok",
                    DialogInterface.OnClickListener { dialog, id ->
                        
                    })
            }.create().show()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentHomeBinding>(inflater,R.layout.fragment_home,container,false)
        binding.progressBar.isVisible=true


        postRecyclerView = binding.postsList
        postRecyclerView.layoutManager = LinearLayoutManager(thisactivity)
        postRecyclerView.setHasFixedSize(true)

        viewModel.postArrayList.observe(viewLifecycleOwner, Observer { newPostArrayList ->
            binding.progressBar.isVisible=false
            postRecyclerView.adapter = viewModel.getAdapter(newPostArrayList)
        })

        return binding.root
    }


    override fun onStop() {
        super.onStop()
        var isAnswered = 2
//        val activity = activity as MainActivity
        val sharedPref = thisactivity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null) {
            isAnswered = sharedPref.getString("survey",0.toString()).toString().toInt()
        }

        if (connectionHelper.checkForInternet(context)) {

            if(isAnswered==0) {
                val singleItems = arrayOf("1", "2", "3", "4", "5", "6", "7", "8", "9", "10")
                val checkedItem = 1
                var grade = 0
                var save=1

                MaterialAlertDialogBuilder(requireContext())
                    .setTitle("How much do you like the news feed?")
                    .setNeutralButton("Cancel") { dialog, which ->
                        // Respond to neutral button press
                    }
                    .setPositiveButton("ok") { dialog, which ->
                        // Respond to positive button presst
                        viewModel.addUserSurvey(grade)
                        if(sharedPref != null) {
                            with (sharedPref.edit()) {
                                putString("survey", 1.toString())
                                apply()
                            }
                        }

                    }
                    // Single-choice items (initialized with checked item)
                    .setSingleChoiceItems(singleItems, checkedItem) { dialog, which ->
                        // Respond to item chosen
                        grade = which.toInt() + 1

                    }
                    .show()
            }
       }
    }

    private fun hasPermissions(context: Context, vararg permissions: String): Boolean = permissions.all{
        ActivityCompat.checkSelfPermission(context,it) == PackageManager.PERMISSION_GRANTED
    }

    private fun handleInformationMessage(newInformationMessage: String?){
        var quantity = "startup"
        if (viewModel.counter != 1)
        {
            quantity="startups"
        }
        binding.startupsInLocation.setText("There are " + newInformationMessage + " "+quantity +" in the country you are!")
        binding.locationLayout.isVisible=true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == viewModel.COARSE_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED){
                Toast.makeText(requireContext(), "Coarse Location Permission Granted", Toast.LENGTH_SHORT) .show()
            }
            else{
                Toast.makeText(requireContext(), "Coarse Location Permission Granted", Toast.LENGTH_SHORT) .show()
            }
        }
        if (requestCode == viewModel.FINE_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED){
                Toast.makeText(requireContext(), "Fine Location Location Permission Granted", Toast.LENGTH_SHORT) .show()
            }
            else{
                Toast.makeText(requireContext(), "Fine Location Permission Denied", Toast.LENGTH_SHORT) .show()
            }
        }
    }

    fun swapToProfileFragment(){
        var see = parentFragment

        when(see){
            is NavHostFragment -> see = see.parentFragment
            else -> { }
        }

        when(see){
            is GenericDashboardFragment -> see.swapToProfileFragment()
            else -> {}
        }
    }

    fun swapToSearchFragment(){
        var see = parentFragment

        when(see){
            is NavHostFragment -> see = see.parentFragment
            else -> { }
        }

        when(see){
            is GenericDashboardFragment -> see.swapToSearchFragment()
            else -> {}
        }
    }


}