package com.team20.unicorn

import android.app.AlertDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.stripe.android.model.ConfirmPaymentIntentParams
import com.stripe.android.payments.paymentlauncher.PaymentLauncher
import com.stripe.android.payments.paymentlauncher.PaymentResult
import com.stripe.android.view.CardInputWidget
import com.team20.unicorn.databinding.FragmentPromoteBinding
import com.team20.unicorn.viewModel.PromoteViewModel
import com.team20.unicorn.viewModel.PromoteViewModelFactory
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PromoteFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PromoteFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var viewModel: PromoteViewModel
    private lateinit var viewModelFactory: PromoteViewModelFactory
    private lateinit var paymentLauncher: PaymentLauncher
    private lateinit var pepeto: CardInputWidget

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        paymentLauncher = PaymentLauncher.Companion.create(
            this,
            "pk_test_51K1zfWCLAaGpJJL2hfKF9kJzRnSIzv21kvzFtmI5FaQItnvflsQoZpi4WsqU24dowBPx3VAOvDvWjwZSgFb2vZkk00K5FEkyt2",
            "acct_1K1zfWCLAaGpJJL2",
            ::onPaymentResult
        )

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentPromoteBinding>(
            inflater,
            R.layout.fragment_promote,
            container,
            false
        )


        val activity = activity as MainActivity
        var id = ""

        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if (sharedPref != null) {
            id = sharedPref.getString("userId", "").toString()
        }

        viewModelFactory = PromoteViewModelFactory(id)

        viewModel = ViewModelProvider(activity, viewModelFactory)
            .get(PromoteViewModel::class.java)

        binding.confirmPayment.setOnClickListener {
            viewModel.getCustomerCode(activity as Context)
        }

        pepeto = binding.pepeto

        viewModel.customerKey.observe(viewLifecycleOwner, Observer { code ->
            startCheckout(code)
        })

        return binding.root
    }

    private fun displayAlert(
        title: String,
        message: String,
    ) {
        val builder = AlertDialog.Builder(activity)
            .setTitle(title)
            .setMessage(message)
        builder.setPositiveButton("Ok", null)
        builder
            .create()
            .show()

        requireView().findNavController().navigate(R.id.action_promoteFragment_to_profileFragment)
    }


    private fun startCheckout(code: String) {
        pepeto.paymentMethodCreateParams?.let { params ->
            val confirmParams = ConfirmPaymentIntentParams
                .createWithPaymentMethodCreateParams(params, code)
            lifecycleScope.launch {
                paymentLauncher.confirm(confirmParams)
            }
        }
    }

    private fun onPaymentResult(paymentResult: PaymentResult) {
        val message = when (paymentResult) {
            is PaymentResult.Completed -> {
                "Completed!"
            }
            is PaymentResult.Canceled -> {
                "Canceled!"
            }
            is PaymentResult.Failed -> {
                // This string comes from the PaymentIntent's error message.
                // See here: https://stripe.com/docs/api/payment_intents/object#payment_intent_object-last_payment_error-message
                "Failed: " + paymentResult.throwable.message
            }
        }
        displayAlert(
            "Payment Result:",
            message,
        )

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PromoteFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PromoteFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}