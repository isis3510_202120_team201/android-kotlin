package com.team20.unicorn

import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.tabs.TabLayout
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.team20.unicorn.databinding.FragmentProfileBinding
import com.team20.unicorn.helper.CustomGlideListener
import com.team20.unicorn.viewModel.*
import com.stripe.android.paymentsheet.PaymentSheet
import com.team20.unicorn.helper.ConnectionHelper

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : Fragment() {

    private lateinit var viewModel: ProfileViewModel
    private lateinit var viewModelFactory: ProfileViewModelFactory
    private var actual : String = ""
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentProfileBinding>(inflater,R.layout.fragment_profile,container,false)
        var id = ""

        PaymentSheet.Configuration(
            merchantDisplayName = "My app, Inc."
        )

        val activity = activity as MainActivity

        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null && sharedPref.getString("tempId","") != "") {
            id = sharedPref.getString("tempId","").toString()
            binding.bttnEditProfile.visibility = View.GONE
            binding.bttnPromote.visibility = View.GONE
        }

        if(sharedPref != null && id == "") {
            id = sharedPref.getString("userId","").toString()
        }

        viewModelFactory = ProfileViewModelFactory(id)

        viewModel = ViewModelProvider(this,viewModelFactory)
            .get(ProfileViewModel::class.java)

        binding.backButtonProfile.setOnClickListener{ view: View ->
            view.findNavController().navigate(R.id.action_profileFragment_to_genericDashboardFragment)
        }

        binding.signOut.setOnClickListener{
            var see = parentFragment

            when(see){
                is NavHostFragment -> see = see.parentFragment
            }

            when(see){
                is MainFragment -> see.logOut()
            }
        }

        viewModel.fullname.observe(viewLifecycleOwner, Observer { name ->
            binding.EntrepName.text = name
        })

        binding.bttnPromote.setOnClickListener{
            requireView().findNavController().navigate(R.id.action_profileFragment_to_promoteFragment)
        }


        binding.bttnEditProfile.setOnClickListener{view ->
            if (sharedPref != null) {
                with(sharedPref.edit()) {
                    remove("tempId")
                    apply()
                }
            }
            view.findNavController()
                .navigate(R.id.action_profileFragment_to_editProfileFragment)
            if(!connectionHelper.checkForInternet(requireContext()))
            {
                val alertDialog = androidx.appcompat.app.AlertDialog.Builder(requireContext())
                alertDialog.apply {
                    setTitle("Internet connection")
                    setMessage("The information will be uploaded when you have Internet connection")
                    setNeutralButton("Ok",
                        DialogInterface.OnClickListener { dialog, id ->

                        })
                }.create().show()
            }

        }

        viewModel.profilePic.observe(viewLifecycleOwner, { profileUrl ->
            if(profileUrl.first != "" || profileUrl.second != ""){
                setPic(binding.imageView3,profileUrl,"users/${viewModel.userId}/profile.jpeg")
            }
        })

        viewModel.bannerPic.observe(viewLifecycleOwner, { profileUrl ->
            if(profileUrl.first != "" || profileUrl.second != ""){
                setPic( binding.imageView, profileUrl, "users/${viewModel.userId}/banner.jpeg")
            }
        })

        actual = "INFO"

        var manager = childFragmentManager
        var see = manager.fragments[0]
        var controller = see.requireView().findNavController()

        binding.profileTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when(tab?.position){
                    0 -> {
                        when(actual){
                            "INTS" -> controller.navigate(R.id.action_interestsFragment_to_informationFragment2)
                            "STARTUPS" -> controller.navigate(R.id.action_startupsFragment_to_informationFragment2)
                            else -> {}
                        }
                        actual = "INFO"
                    }
                    1 -> {
                        when(actual){
                            "INTS" -> controller.navigate(R.id.action_interestsFragment_to_startupsFragment)
                            "INFO" -> controller.navigate(R.id.action_informationFragment2_to_startupsFragment)
                            else -> {}
                        }
                        actual = "STARTUPS"
                    }
                    2 -> {
                        when(actual){
                            "INFO" -> controller.navigate(R.id.action_informationFragment2_to_interestsFragment)
                            "STARTUPS" -> controller.navigate(R.id.action_startupsFragment_to_interestsFragment)
                            else -> {}
                        }
                        actual = "INTS"
                    }
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                // Handle tab reselect
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                // Handle tab unselect
            }
        })

        return binding.root
    }

    private fun setPic(imageView: ImageView, use: Pair<String,String>, url: String){
        var myTrace = Firebase.performance.newTrace("get_image")
        myTrace.start()
        Log.i("IMAGES","url is $url\nuse.first = ${use.first}\nuse.second = ${use.second}")
        Glide.with(this)
            .load(use.first.toUri())
            .listener(CustomGlideListener(myTrace,url,true))
            .error(
                Glide.with(this)
                    .load(use.second.toUri())
                    .listener(CustomGlideListener(null,url,false))
                    .apply(
                        RequestOptions()
                            .placeholder(R.drawable.ic_baseline_add_photo_alternate_24)
                            .error(R.drawable.ic_baseline_warning_24)
                    )
            )
            .centerInside()
            .into(imageView)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProfileFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}