package com.team20.unicorn

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.google.android.material.tabs.TabLayout
import com.team20.unicorn.databinding.FragmentWeeklyHotBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [WeeklyHotFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WeeklyHotFragment : Fragment() {
    private var actual : String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentWeeklyHotBinding>(inflater,R.layout.fragment_weekly_hot,container,false)
        actual = "WEEKLY"

        var manager = childFragmentManager
        var see = manager.fragments[0]
        var controller = see.requireView().findNavController()

        binding.weeklyTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when(tab?.position){
                    0 -> {
                        when(actual){
                            "HOT" -> controller.navigate(R.id.action_hotFragment2_to_weeklyFragment3)
                            else -> {}
                        }
                        actual = "WEEKLY"
                    }
                    1 -> {
                        when(actual){
                            "WEEKLY" -> controller.navigate(R.id.action_weeklyFragment3_to_hotFragment2)
                            else -> {}
                        }
                        actual = "HOT"
                    }

                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                // Handle tab reselect
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                // Handle tab unselect
            }
        })
        return binding.root
    }


}