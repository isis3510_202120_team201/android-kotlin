package com.team20.unicorn.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SearchUserModelFactory(var userId: String = ""): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchUserViewModel::class.java)) {
            return SearchUserViewModel(userId) as T
        }
        throw IllegalArgumentException("Unknow ViewModel class")
    }
}
