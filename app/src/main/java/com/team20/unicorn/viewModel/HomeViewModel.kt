package com.team20.unicorn.viewModel

import android.Manifest
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.database.*
import com.team20.unicorn.helper.MyAdapter
import com.team20.unicorn.repository.database.FirestoreService
import com.team20.unicorn.model.Post
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel(var userId: String = ""): ViewModel() {
    val COARSE_PERMISSION_CODE = 100
    val FINE_PERMISSION_CODE = 101
    var counter: Int = 0
    var country =""
    private lateinit var dbref: DatabaseReference
    private lateinit var adapter: MyAdapter

    private val _postArrayList = MutableLiveData<ArrayList<Post>>()
    val postArrayList: LiveData<ArrayList<Post>>
        get() = _postArrayList

    var imagesArrayList = ArrayList<MutableLiveData<String>>()

    val PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
    )
    private val _informationMessage = MutableLiveData<String>()
    val informationMessage: LiveData<String>
        get() = _informationMessage

    init{
        getPostData()
    }

    fun getPostData() {
        viewModelScope.launch(Dispatchers.IO){
            val temp = FirestoreService.getPosts()
            if(temp != null){
                imagesArrayList = ArrayList<MutableLiveData<String>>()
                repeat(temp.size){
                    imagesArrayList.add(MutableLiveData<String>())
                }
            }
            _postArrayList.postValue(temp)
        }
    }

    fun getStartUpData() {
        viewModelScope.launch {
            var ans = withContext(Dispatchers.IO){
                FirestoreService.getStartupsInCountry(country)
            }
            Log.i("QUERYANS","ans is $ans")
            _informationMessage.value = ans.toString()
        }
    }


    fun addUserSurvey(grade: Int){
        FirestoreService.addSurveyAnswer(userId, grade, 1)

    }

    fun getAdapter(array: ArrayList<Post>): MyAdapter{
        return MyAdapter(array)
    }
}