package com.team20.unicorn.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.team20.unicorn.model.SimpleEvent
import com.team20.unicorn.repository.database.FirestoreService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CalendarViewModel(var colorEvt: Int) : ViewModel() {

    private val _eventsList = MutableLiveData<HashMap<Int,SimpleEvent>>()
    val eventsList: LiveData<HashMap<Int,SimpleEvent>>
        get() = _eventsList

    lateinit var eventsLists: HashMap<Int,SimpleEvent>

    init{
        Log.i("GAL", "init")
    }
    fun getEvents(color: Int){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                _eventsList.postValue(FirestoreService.getEvents(color))
            }
        }
    }


}