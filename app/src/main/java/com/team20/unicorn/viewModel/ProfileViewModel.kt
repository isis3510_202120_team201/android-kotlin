package com.team20.unicorn.viewModel

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.team20.unicorn.repository.database.FirestoreService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.time.LocalDateTime

class ProfileViewModel (var userId: String = ""):ViewModel() {
    private val _fullname = MutableLiveData<String>()
    val fullname: LiveData<String>
        get() = _fullname

    private val _firstname = MutableLiveData<String>()
    val name: LiveData<String>
        get() = _firstname

    private val _lastname = MutableLiveData<String>()
    val lastname: LiveData<String>
        get() = _lastname

    private val _country = MutableLiveData<String>()
    val country: LiveData<String>
        get() = _country

    private val _email = MutableLiveData<String>()
    val email: LiveData<String>
        get() = _email

    private val _linkedInProf = MutableLiveData<String>()
    val linkedInProf: LiveData<String>
        get() = _linkedInProf

    private val _interests = MutableLiveData<MutableList<String>>()
    val interests: LiveData<MutableList<String>>
        get() = _interests

    private val _profilePic = MutableLiveData<Pair<String,String>>()
    val profilePic: LiveData<Pair<String,String>>
        get() = _profilePic

    private val _bannerPic = MutableLiveData<Pair<String,String>>()
    val bannerPic: LiveData<Pair<String,String>>
        get() = _bannerPic

    private val _userType = MutableLiveData<String>()
    val userType: LiveData<String>
        get() = _userType

    private val _customerKey = MutableLiveData<String>()
    val customerKey: LiveData<String>
        get() = _customerKey

    var bannerUri: Uri = Uri.EMPTY
    var profileUri: Uri = Uri.EMPTY
    var profileBmp: Bitmap? = null
    var bannerBmp: Bitmap? = null
    var caller: Boolean = false

    var updateImages: Boolean = false

    inline fun <T> sdk29AndUp(onSdk29: () -> T): T? {
        return if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            onSdk29()
        } else null
    }

    init {
        getUser(userId)
    }

    fun getUser(userId: String){
        viewModelScope.launch {
            var ans = withContext(Dispatchers.IO){
                FirestoreService.getUser(userId)
            }
            _firstname.value = ans?.firstName?:""
            _lastname.value = ans?.lastName?:""
            _fullname.value = _firstname.value + " "+ _lastname.value
            _email.value = ans?.email?:""
            _linkedInProf.value = ans?.linkedInProfile?:"You haven't added a LinkedInProfile"
            _interests.value = ans?.interests?:mutableListOf("No interests chosen")
            if(_linkedInProf.value.toString().isEmpty()){
                _linkedInProf.value = "You haven't added a LinkedInProfile"
            }
            if(ans != null && ans.id != null){
                _profilePic.value = FirestoreService.getImage("users/${ans.id}/profile.jpeg",
                    ans.id!!
                )
                _bannerPic.value = FirestoreService.getImage("users/${ans.id}/banner.jpeg",
                    ans.id!!
                )
            }
            else{
                _bannerPic.value = Pair("","")
                _profilePic.value = Pair("","")
            }
            _userType.value= ans?.type?:""
        }
    }

    fun updateProfile(newFirstName: String, newLastName: String, newLinkedInProf: String,
        profilePic: String, bannerPic: String){
        MainScope().launch(Dispatchers.IO) {
            // TODO: Change this please
            FirestoreService.updateProfile(userId,newFirstName,newLastName,newLinkedInProf,profilePic, bannerPic,"",false)
        }
    }

    fun resizeBitmap(source: Bitmap, maxLength: Int): Bitmap {
        try {
            if (source.height >= source.width) {
                if (source.height <= maxLength) { // if image height already smaller than the required height
                    return source
                }
                val aspectRatio = source.width.toDouble() / source.height.toDouble()
                val targetWidth = (maxLength * aspectRatio).toInt()
                val result = Bitmap.createScaledBitmap(source, targetWidth, maxLength, false)
                return result
            } else {
                if (source.width <= maxLength) { // if image width already smaller than the required width
                    return source
                }

                val aspectRatio = source.height.toDouble() / source.width.toDouble()
                val targetHeight = (maxLength * aspectRatio).toInt()

                val result = Bitmap.createScaledBitmap(source, maxLength, targetHeight, false)
                return result
            }
        } catch (e: Exception) {
            return source
        }
    }

    fun savePhotoToExternalStorage(displayName: String, bmp: Bitmap, context: Context): Uri {
        var answer = Uri.EMPTY
        val imageCollection = sdk29AndUp {
            MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
        } ?: MediaStore.Images.Media.EXTERNAL_CONTENT_URI

        val contentValues = ContentValues().apply {
            put(MediaStore.Images.Media.DISPLAY_NAME, "$displayName.jpg")
            put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            put(MediaStore.Images.Media.WIDTH, bmp.width)
            put(MediaStore.Images.Media.HEIGHT, bmp.height)
        }
        try {
            context.contentResolver.insert(imageCollection, contentValues)?.also { uri ->
                answer = uri
                context.contentResolver.openOutputStream(uri).use { outputStream ->
                    if(!bmp.compress(Bitmap.CompressFormat.JPEG, 95, outputStream)) {
                        throw IOException("Couldn't save bitmap")
                    }
                }
            } ?: throw IOException("Couldn't create MediaStore entry")
            true
        } catch(e: IOException) {
            Log.i("saving","${e.message}")
            e.printStackTrace()
            false
        }
        return answer
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getCustomerCode(context: Context){
        viewModelScope.launch(Dispatchers.IO) {
            val current = LocalDateTime.now()
            val temp = current.plusDays(10)

            val url = "http://192.168.0.12:3000/api/stripe/generatePayment"

            val postData = JSONObject()
            postData.put("id",userId)
            postData.put("startDate",current.toString())
            postData.put("finishDate",temp.toString())

            val stringRequest: JsonObjectRequest = object : JsonObjectRequest( Method.POST, url, postData,
                Response.Listener { response ->
                    try {
                        val payment = response.get("payment") as String
                        Log.i("STRIPE","what is payment $payment")
                        _customerKey.postValue(payment)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show()
                }) {
                override fun getParams(): Map<String, String> {
                    val params: MutableMap<String, String> = HashMap()
                    //Change with your post params
                    params["id"] = userId
                    params["startDate"] = current.toString()
                    params["finishDate"] = temp.toString()
                    return params
                }
            }
            val queue = Volley.newRequestQueue(context)
            queue.add(stringRequest)
        }
    }

}