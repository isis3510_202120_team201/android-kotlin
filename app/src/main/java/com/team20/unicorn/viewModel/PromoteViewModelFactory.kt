package com.team20.unicorn.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PromoteViewModelFactory(var userId: String = ""): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PromoteViewModel::class.java)) {
            return PromoteViewModel(userId) as T
        }
        throw IllegalArgumentException("Unknow ViewModel class")
    }
}