package com.team20.unicorn.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.team20.unicorn.repository.database.FirestoreService
import com.team20.unicorn.model.User
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class GetStartedViewModel: ViewModel() {

    private val _successAuth = MutableLiveData<Boolean>()
    val successAuth : LiveData<Boolean>
        get() = _successAuth

    private val _successCreate = MutableLiveData<Boolean>()
    val successCreate : LiveData<Boolean>
        get() = _successCreate

    private val _dialogMessage = MutableLiveData<String>()
    val dialogMessage: LiveData<String>
        get() = _dialogMessage


    var toTwoFactor : Boolean = false

    var userId: String = ""

    var auth : FirebaseAuth

    init{
        auth = Firebase.auth
    }

    suspend fun registerUser(email:String, password:String): String{
        try {
            var task = auth.createUserWithEmailAndPassword(email, password)
            task.await()
            var answer = ""
            if (task.isSuccessful) {
                var user = auth.currentUser
                answer = user?.uid ?: ""
            }
            return answer
        }catch (e: Exception){
            _dialogMessage.value="We couldn't create the user. " +e.message
            return ""
        }
    }

    fun createUser(firstName: String, lastName: String, email:String, password:String,
        phoneNumber: String, twoFactor: Boolean){
        viewModelScope.launch {
            userId = registerUser(email, password)
            if(userId != ""){
                _successAuth.value = true
                val newuser = User(userId, firstName, lastName, email, phoneNumber = phoneNumber, twoFactor = twoFactor)
                val ans = FirestoreService.createUser(userId, newuser)
                toTwoFactor = twoFactor
                _successCreate.value = ans
            }
        }
    }
}
