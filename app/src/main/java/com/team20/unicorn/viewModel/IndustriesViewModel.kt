package com.team20.unicorn.viewModel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.team20.unicorn.model.Industry
import org.json.JSONObject
import org.json.JSONTokener


class IndustriesViewModel : ViewModel()
{
    val serverURL = "https://unicornm.herokuapp.com/"
    private val _industriesArrayList = MutableLiveData<ArrayList<Industry>>()
    val industriesArrayList: LiveData<ArrayList<Industry>>
        get() = _industriesArrayList


    fun getData(requireContext: Context) {
        val industriesArray = ArrayList<Industry>()
        val queue = Volley.newRequestQueue(requireContext)
        // Request a string response from the provided URL.
        val stringRequest = StringRequest(Request.Method.GET, serverURL+"api/inversion/all",
            { response ->
                val jsonObject = JSONTokener(response).nextValue() as JSONObject
                val data = jsonObject.get("data") as JSONObject
                val keys=data.keys()
                var industry :String
                var item :JSONObject
                var objTem:Industry
                while(keys.hasNext())
                {
                    industry=keys.next()
                    item=data.getJSONObject(industry)
                    objTem= Industry(industry, item.get("funding").toString().toLong(), item.get("startups").toString().toInt())
                    industriesArray.add(objTem)

                }
                _industriesArrayList.value=industriesArray
            },
            { })

        // Add the request to the RequestQueue.
        queue.add(stringRequest)

    }

}
