package com.team20.unicorn.viewModel

import android.content.ContentValues
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.team20.unicorn.repository.database.FirestoreService
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class SignInViewModel: ViewModel() {
    private var auth: FirebaseAuth
    var userId: String = ""

    private val _success = MutableLiveData<Boolean>()
    val success : LiveData<Boolean>
        get() = _success
    private val _dialogMessage = MutableLiveData<String>()
    val dialogMessage: LiveData<String>
        get() = _dialogMessage

    var twoFactor : Boolean = false
    var phoneNumber: String? = null

    init{
        auth = Firebase.auth
    }

    fun signIn(email:String, password:String){
        viewModelScope.launch{
                val myTrace = Firebase.performance.newTrace("signin_trace")
            try {
                myTrace.start()
                val task = auth.signInWithEmailAndPassword(email, password)
                task.await()
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(ContentValues.TAG, "signInWithEmail:success")
                    val user = auth.currentUser
                    if (user != null)
                        userId = user.uid

                    var tempUser = FirestoreService.getUser(userId)
                    phoneNumber = tempUser?.phoneNumber
                    twoFactor = tempUser?.twoFactor?:false
                    _success.value = true
                }
            else{
                // If sign in fails, display a message to the user.
                Log.w(ContentValues.TAG, "signInWithEmail:failure", task.exception)
                _success.value = false
            }
            }
            catch (e: Exception)
            {
                if(e.message.toString() == "There is no user record corresponding to this identifier. The user may have been deleted.")
                {
                    _dialogMessage.value="There is no user with this email."
                }
                else if (e.message.toString() =="The password is invalid or the user does not have a password.")
                {
                    _dialogMessage.value="The password is incorrect for this email."
                }
                else if (e.message.toString() =="A network error (such as timeout, interrupted connection or unreachable host) has occurred.")
                {
                    _dialogMessage.value="To login to Unicorn you need internet connection."
                }
                else
                {
                    _dialogMessage.value="We couldn't sing in. "+ e.message
                }
                System.out.println("Llega catch " + e.message)
            }
            myTrace.stop()
        }
    }
}