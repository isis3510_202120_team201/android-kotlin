package com.team20.unicorn.viewModel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.team20.unicorn.model.Trend
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener

class TrendsViewModel : ViewModel() {
    val serverURL = "https://unicornm.herokuapp.com/"
    private val _trendsArrayList = MutableLiveData<ArrayList<Trend>>()
    val trendsArrayList: LiveData<ArrayList<Trend>>
        get() = _trendsArrayList

    private val _countriesArrayList = MutableLiveData<ArrayList<String>>()
    val countryArrayList: LiveData<ArrayList<String>>
        get() = _countriesArrayList


    fun getCountries (context: Context)
    {
        val countriesArray = ArrayList<String>()

        val queue = Volley.newRequestQueue(context)
        // Request a string response from the provided URL.
        val stringRequest = StringRequest(Request.Method.GET, serverURL+"api/country/all",
            { response ->
                val jsonObject = JSONTokener(response).nextValue() as JSONObject
                val data = jsonObject.get("data") as JSONArray
                val length= data.length()
                var i=0
                while(i<length)
                {
                    countriesArray.add(data[i] as String)
                    i++
                }
                _countriesArrayList.value = countriesArray

            },
            { })

        // Add the request to the RequestQueue.
        queue.add(stringRequest)

    }

    fun getPerCountry(country: String, context: Context)
    {
        val trendsArray = ArrayList<Trend>()
        val queue = Volley.newRequestQueue(context)
        // Request a string response from the provided URL.
        val stringRequest = StringRequest(Request.Method.GET, serverURL+"api/trending/country/"+country,
            { response ->
                val jsonObject = JSONTokener(response).nextValue() as JSONObject
                val data = jsonObject.get("data") as JSONObject
                val keys=data.keys()
                var trend:String
                var item : JSONObject
                var objTem: Trend
                while(keys.hasNext())
                {
                    trend=keys.next()
                    item=data.getJSONObject(trend)
                    objTem= Trend(trend, item.get("funding").toString().toLong(), item.get("startups").toString().toInt())
                    trendsArray.add(objTem)

                }
                _trendsArrayList.value = trendsArray
            },
            { })

        // Add the request to the RequestQueue.
        queue.add(stringRequest)
    }

}