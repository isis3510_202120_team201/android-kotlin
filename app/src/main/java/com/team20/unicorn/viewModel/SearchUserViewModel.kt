package com.team20.unicorn.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.team20.unicorn.helper.SearchAdapter
import com.team20.unicorn.model.User
import com.team20.unicorn.repository.database.FirestoreService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchUserViewModel(var userId: String = "") : ViewModel() {
    private val _usersArrayList = MutableLiveData<ArrayList<User>>()
    val usersArrayList: LiveData<ArrayList<User>>
        get() = _usersArrayList

    private val _toGoUser = MutableLiveData<String>()
    val toGoUser: LiveData<String>
        get() = _toGoUser

    var imagesArrayList = ArrayList<MutableLiveData<String>>()

    init{
        getPostData()
    }

    fun getPostData() {
        viewModelScope.launch(Dispatchers.IO){
            val temp = FirestoreService.getSuggestedUsersPosts(userId)
            if(temp != null){
                imagesArrayList = ArrayList<MutableLiveData<String>>()
                repeat(temp.size){
                    imagesArrayList.add(MutableLiveData<String>())
                }
            }
            _usersArrayList.postValue(temp)
        }
    }

    fun getAdapter(array: ArrayList<User>): SearchAdapter {
        return SearchAdapter(array,this)
    }

    fun goToProfile(toGoUserId: String){
        _toGoUser.value = toGoUserId
    }

}
