package com.team20.unicorn.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CalendarViewModelFactory (var colorEvt: Int = 0): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(CalendarViewModel::class.java)){
            return CalendarViewModel(colorEvt) as T
        }
        throw IllegalArgumentException("Unknow ViewModel class")
    }
}