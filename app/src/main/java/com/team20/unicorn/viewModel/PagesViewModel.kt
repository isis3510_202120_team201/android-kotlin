package com.team20.unicorn.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.team20.unicorn.model.StartupPage
import com.team20.unicorn.repository.database.FirestoreService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PagesViewModel: ViewModel() {
    var userId: String = ""

    private val _successCreate = MutableLiveData<Boolean>()
    val successCreate: LiveData<Boolean>
        get() = _successCreate
    private var _pagesArrayList = MutableLiveData<ArrayList<StartupPage>>()
    val pagesArrayList: LiveData<ArrayList<StartupPage>>
        get() = _pagesArrayList

    private val _toGoWeekly = MutableLiveData<String>()
    val toGoWeekly: LiveData<String>
        get() = _toGoWeekly

    private var _page = MutableLiveData<StartupPage>()
    val page: LiveData<StartupPage>
        get() = _page

    fun addPage(startupPage: StartupPage){
        viewModelScope.launch {
            var ans = withContext(Dispatchers.IO){
                FirestoreService.addPage(startupPage)

            }
            _successCreate.value=ans
        }

    }

    fun getPagesWeekly() {
        viewModelScope.launch(Dispatchers.IO) {

            val task = async { FirestoreService.getPromoteds() }
            val ans = task.await()
            viewModelScope.launch(Dispatchers.Main) {
                _pagesArrayList.value = ans
            }

        }
    }

    fun getPagesById(userId: String)
    {
        viewModelScope.launch(Dispatchers.IO) {

            val task = async { FirestoreService.getPagesByUID(userId) }
            val ans = task.await()
            viewModelScope.launch(Dispatchers.Main) {
                if(ans!=null)
                {
                    _pagesArrayList.value = ans!!
                }
            }

        }
    }

    fun updateWeekly(weekly: String){
        _toGoWeekly.value = weekly
    }


    fun updateViews(userId: String)
    {
        viewModelScope.launch(Dispatchers.IO) {
            FirestoreService.updateViews(userId)
        }
    }

    fun getPageById(pageId: String)
    {
        viewModelScope.launch(Dispatchers.IO) {

            val task = async { FirestoreService.getPageById(pageId) }
            val ans = task.await()
            viewModelScope.launch(Dispatchers.Main) {
                if(ans!=null)
                {
                    _page.value = ans
                }
            }

        }
    }

}