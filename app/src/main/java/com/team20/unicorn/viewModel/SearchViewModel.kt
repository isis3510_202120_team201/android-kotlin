package com.team20.unicorn.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.team20.unicorn.repository.database.FirestoreService
import kotlinx.coroutines.launch

class SearchViewModel(val userID: String): ViewModel() {

    private val _profilePic = MutableLiveData<Pair<String,String>>()
    val profilePic : LiveData<Pair<String,String>>
        get() = _profilePic

    init{
        getUserPic()
    }

    private fun getUserPic(){
        viewModelScope.launch {
            var path = FirestoreService.getImage("users/$userID/profile.jpeg",userID)
            _profilePic.value = path
        }
    }
}