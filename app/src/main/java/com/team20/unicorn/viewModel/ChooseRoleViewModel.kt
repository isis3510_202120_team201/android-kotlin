package com.team20.unicorn.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.team20.unicorn.repository.database.FirestoreService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ChooseRoleViewModel(var userId: String = "") : ViewModel() {

    private val _success = MutableLiveData<Boolean>()
    val success : LiveData<Boolean>
        get() = _success

    init{
    }
    fun addRole(userType: String){
        viewModelScope.launch {
            var ans = withContext(Dispatchers.IO){
                FirestoreService.addRole(userId, userType)
            }
            _success.value = ans
        }

    }

}