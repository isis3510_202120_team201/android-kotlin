package com.team20.unicorn.viewModel

import android.content.Context
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import java.time.LocalDateTime

class PromoteViewModel (var userId: String = ""): ViewModel() {
    val serverURL = "https://unicornm.herokuapp.com/"

    private val _customerKey = MutableLiveData<String>()
    val customerKey: LiveData<String>
        get() = _customerKey

    @RequiresApi(Build.VERSION_CODES.O)
    fun getCustomerCode(context: Context){
        viewModelScope.launch(Dispatchers.IO) {
            val current = LocalDateTime.now()
            val temp = current.plusDays(10)

            val url = serverURL+ "api/stripe/generatePayment"

            val postData = JSONObject()
            postData.put("id",userId)
            postData.put("startDate",current.toString())
            postData.put("finishDate",temp.toString())

            val stringRequest: JsonObjectRequest = object : JsonObjectRequest( Method.POST, url, postData,
                Response.Listener { response ->
                    try {
                        val payment = response.get("payment") as String
                        Log.i("STRIPE","what is payment $payment")
                        _customerKey.postValue(payment)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show()
                }) {
                override fun getParams(): Map<String, String> {
                    val params: MutableMap<String, String> = HashMap()
                    //Change with your post params
                    params["id"] = userId
                    params["startDate"] = current.toString()
                    params["finishDate"] = temp.toString()
                    return params
                }
            }
            val queue = Volley.newRequestQueue(context)
            queue.add(stringRequest)
        }
    }
}