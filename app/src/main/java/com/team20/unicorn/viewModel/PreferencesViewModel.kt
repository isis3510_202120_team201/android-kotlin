package com.team20.unicorn.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.team20.unicorn.repository.database.FirestoreService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PreferencesViewModel (var userId: String = ""): ViewModel()  {

    private val _success = MutableLiveData<Boolean>()
    val success : LiveData<Boolean>
        get() = _success

    init{
    }
    val userInts :MutableMap<String, Boolean> =
        mutableMapOf("Crypto" to false, "Blockchain" to false, "Fintech" to false, "Innovation" to false, "Random" to false)

    fun addInterests(userIntsDef: MutableList<String>){
        viewModelScope.launch {
            var ans = withContext(Dispatchers.IO){
                FirestoreService.addInterests(userId,userIntsDef)
            }
            _success.value = ans
        }

    }
}
