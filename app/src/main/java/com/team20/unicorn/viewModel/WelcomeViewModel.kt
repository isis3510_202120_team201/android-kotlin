package com.team20.unicorn.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WelcomeViewModel: ViewModel() {
    private var auth: FirebaseAuth

    private val _logged = MutableLiveData<Boolean>()
    val logged : LiveData<Boolean>
        get() = _logged

    init{
        auth = Firebase.auth
        checkLogging()
    }

    fun checkLogging() = viewModelScope.launch(Dispatchers.IO) {
        _logged.postValue(auth.currentUser != null)
    }

}