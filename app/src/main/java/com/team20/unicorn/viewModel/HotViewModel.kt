package com.team20.unicorn.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.team20.unicorn.repository.database.FirestoreService
import kotlinx.coroutines.launch

class HotViewModel : ViewModel() {

    private val _fundingType = MutableLiveData<MutableMap<String,Int>>()
    val fundingType: LiveData<MutableMap<String,Int>>
        get() = _fundingType

    private val _ico = MutableLiveData<MutableMap<String,Int>>()
    val ico: LiveData<MutableMap<String,Int>>
        get() = _ico

    init {
        getInfo()
    }

    fun getInfo(){
        viewModelScope.launch{
            _ico.value = FirestoreService.queryICO()
        }
        viewModelScope.launch {
            _fundingType.value = FirestoreService.queryPreferredFounding()
        }
    }
}