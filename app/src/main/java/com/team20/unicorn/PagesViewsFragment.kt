package com.team20.unicorn

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.team20.unicorn.databinding.FragmentPagesViewBinding
import com.team20.unicorn.helper.ConnectionHelper
import com.team20.unicorn.helper.PagesAdapter
import com.team20.unicorn.viewModel.PagesViewModel

/**
 * A simple [Fragment] subclass.
 * Use the [PagesViewsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PagesViewsFragment : Fragment() {

    private lateinit var viewModel: PagesViewModel
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }
    private  lateinit var weekliesRecyclerView: RecyclerView
    private lateinit var btnCreate: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PagesViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentPagesViewBinding>(inflater,R.layout.fragment_pages_view,container,false)
        val activity = activity as MainActivity
        var id =""
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null && id == "") {
            id = sharedPref.getString("userId","").toString()
        }
        btnCreate=binding.btnCreatePage
        binding.progressBarPagesView.isVisible=true
        viewModel.getPagesById(id)
        weekliesRecyclerView = binding.pagesViewList
        weekliesRecyclerView.layoutManager = LinearLayoutManager(activity)
        viewModel.pagesArrayList.observe(viewLifecycleOwner, Observer { newArrayList ->
            binding.progressBarPagesView.isVisible = false
            if(newArrayList.isEmpty())
            {
                binding.noPages.isVisible=true
            }
            else {
                weekliesRecyclerView.adapter = PagesAdapter(newArrayList,viewModel,false)
            }
        })
        btnCreate.setOnClickListener { view: View ->
            if (connectionHelper.checkForInternet(requireContext())) {
                var see = parentFragment

                when (see) {
                    is NavHostFragment -> see = see.parentFragment
                    else -> {}
                }

                when (see) {
                    is GenericDashboardFragment -> see.swapToPageFragment()
                    else -> {}
                }
            }
            else
            {
                val alertDialog = AlertDialog.Builder(requireContext())
                 alertDialog.apply {
                 setTitle("Internet connection")
                 setMessage("You don't have internet.To use the functionality of create page you need internet connection.")
                 setNeutralButton("Ok",
                            DialogInterface.OnClickListener { dialog, id ->
                            })
                    }.create().show()

            }

        }
        if (!connectionHelper.checkForInternet(requireContext())) {
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.apply {
                setTitle("Internet connection")
                setMessage("You don't have internet connection. We are showing cached info in your phone.")
                setNeutralButton("Ok",
                    DialogInterface.OnClickListener { dialog, id ->
                    })
            }.create().show()
        }

        return binding.root
    }


}