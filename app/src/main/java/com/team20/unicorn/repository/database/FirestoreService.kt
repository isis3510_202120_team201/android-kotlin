package com.team20.unicorn.repository.database

import android.net.Uri
import android.util.Log
import androidx.core.net.toUri
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestoreSettings
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.google.firebase.storage.FirebaseStorage
import com.team20.unicorn.helper.CalendarUtils
import com.team20.unicorn.model.Post
import com.team20.unicorn.model.SimpleEvent
import com.team20.unicorn.model.StartupPage
import com.team20.unicorn.model.User
import com.team20.unicorn.repository.cache.CacheRoutes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.Exception

object FirestoreService {

    private lateinit var db: FirebaseFirestore
    private var TAG = "FirestoreService"
    private val foundingTypes: List<String>

    init {
        foundingTypes = listOf("Bank funding","Business angel","Venture capital","Government","Crowdfunding")
        createDB()
    }

    private fun createDB() {
        db = FirebaseFirestore.getInstance()
        var maxMem = (Runtime.getRuntime().maxMemory() / 10)
        val settings = firestoreSettings {
            isPersistenceEnabled = true
            setCacheSizeBytes(maxMem)
        }
        db.firestoreSettings = settings

        Log.i(TAG, "Firestore initialized with settings!")
    }

    suspend fun createUser(uid: String, user: User): Boolean {
        if (db != null) {
            Log.i(TAG, "creating user")
            var task = db.collection("users").document(uid).set(user)
            task.await()
            if (task.isSuccessful) {
                Log.i(TAG, "created user")
                return true
            } else {
                Log.i(TAG, "failed to create user")
                return false
            }
        } else
            return false
    }

    suspend fun addRole(uid: String, userType: String): Boolean {
        if (db != null) {
            Log.i(TAG, "updating user")
            var task = db.collection("users").document(uid).update("type", userType)
            task.await()
            if (task.isSuccessful) {
                Log.i(TAG, "updated user")
                return true
            } else {
                Log.i(TAG, "failed to update user")
                return false
            }
        } else
            return false

    }

    suspend fun addInterests(uid: String, userInts: List<String>): Boolean {
        if (db != null) {
            Log.i(TAG, "updating user")
            var task = db.collection("users").document(uid).update("interests", userInts)
            task.await()
            if (task.isSuccessful) {
                Log.i(TAG, "updated user")
                return true
            } else {
                Log.i(TAG, "failed to update user")
                return false
            }
        } else
            return false
    }

    fun addSurveyAnswer(uid: String, surveyScore: Int, survey: Int) {
        if (db != null) {
            val data = mutableMapOf(
                "surveyScore" to surveyScore,
                "survey" to survey
            ) as Map<String, Any>
            Log.i("GAL", "user id received" + uid)
            db.collection("users").document(uid).update(data)
        }
    }

    suspend fun getPosts(): ArrayList<Post> {
        val answer = ArrayList<Post>()
        if (db != null) {
            val task =
                db.collection("posts").orderBy("datePosted", Query.Direction.DESCENDING).limit(20)
                    .get()
            val documents = task.await()
            if (task.isSuccessful && documents != null) {
                for (document in documents) {
                    val toAdd = Post(
                        document.id,
                        document.get("owner") as String?,
                        document.get("datePosted") as Timestamp?,
                        document.get("content") as String?,
                        document.get("imgUrl") as String?
                    )
                    answer.add(toAdd)
                    MainScope().launch {
                        if(toAdd.imgUrl != null && toAdd.imgUrl.startsWith("content")){
                            var path = uploadImage("posts/${toAdd.owner}/${document.id}/post.jpeg",toAdd.imgUrl.toUri())
                            if(path != "")
                                updateUriImagePost(document.id,path)
                        }
                    }
                }
            }
        }
        return answer
    }

    suspend fun getUser(uid: String): User? {
        var user: User? = null
        if (db != null) {
            val task = db.collection("users").document(uid).get()
            val document = task.await()
            if (task.isSuccessful && document != null) {
                user = User(
                    id = uid,
                    firstName = document.get("firstName") as String?,
                    lastName = document.get("lastName") as String?,
                    linkedInProfile = document.get("linkedInProfile") as String?,
                    email = document.get("email") as String?,
                    interests = document.get("interests") as MutableList<String>?,
                    type = document.get("type") as String?,
                    bannerPicUrl = document.get("bannerPicUrl") as String?,
                    profilePicUrl = document.get("profilePicUrl") as String?,
                    phoneNumber = document.get("phoneNumber") as String?,
                    twoFactor = document.get("twoFactor") as Boolean?
                )
                MainScope().launch {
                    if(user.profilePicUrl != null && user.profilePicUrl!!.startsWith("content")){
                        var path = uploadImage("users/$uid/profile.jpeg", user.profilePicUrl!!.toUri())
                        if(path != "")
                            updateProfilePic(uid,path)
                    }

                    if(user.bannerPicUrl != null && user.bannerPicUrl!!.startsWith("content")){
                        var path = uploadImage("users/$uid/banner.jpeg", user.bannerPicUrl!!.toUri())
                        if(path != "")
                            updateProfileBannerPic(uid,path)
                    }
                }

            }
        }

        return user
    }

    suspend fun getPost(uid: String): Post?{
        var ans: Post? = null
        if (db != null) {
            val task =
                db.collection("posts").document(uid)
                    .get()
            val document = task.await()
            if (task.isSuccessful && document != null) {
                val toAdd = Post(
                    document.id,
                    document.get("owner") as String?,
                    document.get("datePosted") as Timestamp?,
                    document.get("content") as String?,
                    document.get("imgUrl") as String?
                )
                ans = toAdd
                MainScope().launch(Dispatchers.IO) {
                    if(toAdd.imgUrl != null && toAdd.imgUrl.startsWith("content")){
                        var path = uploadImage("posts/${toAdd.owner}/${document.id}/post.jpeg",toAdd.imgUrl.toUri())
                        if(path != "")
                            updateUriImagePost(document.id,path)
                    }
                }
            }
        }
        return ans
    }

    suspend fun updateProfile(
        uid: String, newFirstName: String, newLastName: String, newLinkedInProf: String,
        profilePic: String, bannerPic: String, phoneNumber: String, twoFactor: Boolean
    ) {
        var data = mutableMapOf(
            "firstName" to newFirstName,
            "lastName" to newLastName,
            "linkedInProfile" to newLinkedInProf,
            "bannerPicUrl" to bannerPic,
            "profilePicUrl" to profilePic,
            "phoneNumber" to phoneNumber,
            "twoFactor" to twoFactor
        ) as Map<String, Any>

        if (db != null) {
            val task = db.collection("users").document(uid).update(data)
            if(profilePic.startsWith("content"))
                CacheRoutes.setKey("users/$uid/profile.jpeg",profilePic)
            if(bannerPic.startsWith("content"))
                CacheRoutes.setKey("users/$uid/banner.jpeg",bannerPic)
            task.addOnSuccessListener {
                MainScope().launch(Dispatchers.IO) {
                    if(profilePic.startsWith("content")) {
                        var path = uploadImage("users/$uid/profile.jpeg", profilePic.toUri())
                        if (path != "")
                            updateProfilePic(uid, path)
                    }
                    if(bannerPic.startsWith("content")) {
                        var path = uploadImage("users/$uid/banner.jpeg", bannerPic.toUri())
                        if (path != "")
                            updateProfileBannerPic(uid, path)
                    }
                }
            }
        }
    }


    suspend fun addPage(startupPage: StartupPage): Boolean {
        var ans = false
        if (db != null) {
            val task = db.collection("pages").add(startupPage)
            val doc = task.await()
            if (task.isSuccessful && doc != null) {
                ans = true
            } else if (task.isCanceled) {
                ans = false
            }
        }
        return ans
    }

    suspend fun getStartupsInCountry(country: String): Int {
        var ans = 0
        Log.i(TAG, "Parameter country: $country")
        if (db != null) {
            val task = db.collection("pages").whereEqualTo("country", country).get()
            val documents = task.await()
            ans = documents.size()
        }
        return ans
    }

    fun publishPost(uid: String, text: String, uri: String){
        var myTrace = Firebase.performance.newTrace("post_$uid")
        myTrace.start()
        if (db != null) {
            Log.i(TAG, "creating post")

            val data = hashMapOf(
                "content" to text,
                "datePosted" to Timestamp.now(),
                "owner" to uid,
                "imgUrl" to uri
            )

            val ref = db.collection("posts").document()
            val task = ref.set(data)
            if(uri.startsWith("content"))
                CacheRoutes.setKey("posts/$uid/${ref.id}/post.jpeg",uri)
            task.addOnSuccessListener {
                Log.i("GAL", "Succeeded adding post")
                MainScope().launch(Dispatchers.IO) {
                    if(uri.startsWith("content")) {
                        var path = uploadImage("posts/$uid/${ref.id}/post.jpeg", uri.toUri())
                        if (path != "")
                            updateUriImagePost(ref.id, path)
                    }
                }
            }.addOnFailureListener {
                Log.i("GAL", "Failed uploading post")
            }
        }
        myTrace.stop()
    }

    fun updateUriImagePost(uid: String, url: String) {
        if (db != null) {
            Log.i(TAG, "creating post")
            val data = hashMapOf(
                "imgUrl" to url
            )
            var task = db.collection("posts").document(uid).update(data as Map<String, Any>)

            task.addOnSuccessListener {
                Log.i("GAL", "Succeeded updated post")
            }.addOnFailureListener {
                Log.i("GAL", "Failed updated post")
            }
        }
    }

    fun updateProfileBannerPic(uid: String, url: String){
        if (db != null) {
            Log.i(TAG, "creating post")
            val data = hashMapOf(
                "bannerPicUrl" to url
            )
            var task = db.collection("users").document(uid).update(data as Map<String, Any>)

            task.addOnSuccessListener {
                Log.i("GAL", "Succeeded updated post")
            }.addOnFailureListener {
                Log.i("GAL", "Failed updated post")
            }
        }
    }

    fun updateProfilePic(uid: String, url: String){
        if (db != null) {
            Log.i(TAG, "creating post")
            val data = hashMapOf(
                "profilePicUrl" to url
            )

            var task = db.collection("users").document(uid).update(data as Map<String, Any>)

            task.addOnSuccessListener {
                Log.i("GAL", "Succeeded updated post")
            }.addOnFailureListener {
                Log.i("GAL", "Failed updated post")
            }
        }
    }

    suspend fun getSuggestedUsersPosts(userId: String): ArrayList<User> {
        val ans = ArrayList<User>()
        val interests = getUser(userId)?.interests ?: ArrayList<String>()
        if (db != null) {
            var task = db.collection("users").whereArrayContainsAny("interests", interests).get()
            val documents = task.await()
            if (task.isSuccessful && documents != null) {
                for (document in documents) {
                    ans.add(
                        User(
                            id = document.id,
                            firstName = document.get("firstName") as String?,
                            lastName = document.get("lastName") as String?,
                            linkedInProfile = document.get("linkedInProfile") as String?,
                            interests = document.get("interests") as MutableList<String>?,
                        )
                    )
                }
            }
        }
        return ans
    }

    suspend fun uploadImage(url: String, uri: Uri): String{
        var ans = ""
        Log.i("UPLOAD","what am i getting? $url $uri")
        var instance = FirebaseStorage.getInstance()
        instance.maxDownloadRetryTimeMillis = 1500
        instance.maxOperationRetryTimeMillis = 1500
        instance.maxUploadRetryTimeMillis = 1500
        try{
            var task = instance.reference.child(url).putFile(uri)
            task.await()
            if(task.isSuccessful){
                var task2 = instance.reference.child(url).downloadUrl
                var temp = task2.await().toString()
                if(task2.isSuccessful)
                    ans = temp
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
        Log.i("UPLOAD","what is afterwards $ans")
        return ans
    }

    suspend fun getImage(path: String, uid: String): Pair<String,String> {
        var ans1 = ""
        var ans2 = ""
        ans1 = CacheRoutes.getKey(path).toString()
        if(path.startsWith("users")){
            if(path.endsWith("banner.jpeg")) ans2 = getUser(uid)?.bannerPicUrl?:""
            else ans2 = getUser(uid)?.profilePicUrl?:""
        }
        else if(path.startsWith("posts")){
            Log.i("IMAGES","what are we getting $path $uid")
            var temp = getPost(uid)
            Log.i("IMAGES","what are we getting then $temp")
            ans2 = getPost(uid)?.imgUrl?:""
        }

        return Pair(ans2, ans1)
    }

    suspend fun getPagesByUID(userId: String): ArrayList<StartupPage>? {
        var ans: ArrayList<StartupPage>? = null

        if (db != null) {
            val task = db.collection("pages")
                .whereEqualTo("ownerUID", userId)
                .get()
            val documents = task.await()
            if (task.isSuccessful && documents != null) {
                    ans=ArrayList<StartupPage>()
                    for (document in documents) {
                        val name = document.get("name")
                        val homepage: String?= document.get("homepage") as String?
                        val phoneNumber: String? = document.get("phoneNumber") as String?
                        val email: String? = document.get("email") as String?
                        val description: String? = document.get("description") as String?
                        val country=document.get("country")
                        val invest: String? = document.get("invest") as String?
                        val preferredFinancial= document.get("preferredFinancial")
                        val ownerUID=document.get("ownerUID")
                        val profilePicUrl: String?=document.get("profilePicUrl") as String?

                        val page: StartupPage= StartupPage(id = document.id,name as String?,homepage,phoneNumber,email,description,
                            country as String?,invest,
                            preferredFinancial as String?,
                            ownerUID as String?,profilePicUrl)
                        ans!!.add(page)
                    }
                }
            }
        return ans
    }

   suspend fun getPromoteds(): ArrayList<StartupPage> {
        var pages: ArrayList<StartupPage> = ArrayList<StartupPage>()
        var pageUsr: ArrayList<StartupPage>? = null
        var promotedUsers : ArrayList<String> = ArrayList<String>()
        if (db != null) {
            val task =
                db.collection("promoted")
                    .get()
            val documents = task.await()
           val task2 =
               MainScope().async {
                   if (task.isSuccessful && documents != null) {
                       if (documents != null) {
                           documents.forEach { item ->
                               val today: Timestamp = Timestamp.now()
                               val finishDate: Timestamp = item.get("finishDate") as Timestamp
                               if (today.compareTo(finishDate) <= 0) {
                                   promotedUsers.add(item.id)
                               }
                           }
                           for (user in promotedUsers) {
                               pageUsr = getPagesByUID(user)
                               if (pageUsr != null) {
                                   for (item in pageUsr!!) {
                                       pages.add(item)
                                   }
                               }
                           }
                       } else {
                           Log.d(TAG, "No such document")
                       }
                   }
               }

            task2.await()
            val ans =pages
            return ans
        }
       else
        {
            return pages
        }
    }

    suspend fun queryPreferredFounding(): MutableMap<String,Int>{
        var ans: MutableMap<String,Int> = mutableMapOf<String,Int>()
        try {
            if (db != null) {
                for (type in foundingTypes) {
                    var task = db.collection("pages").whereEqualTo("preferredFinancial", type).get()
                    var doc = task.await()
                    ans.put(type,doc.size())
                }
            }
        }catch(e: Exception){
            ans = mutableMapOf<String,Int>()
            Log.i(TAG,"there was an error retrying the founding information")
        }
        return ans
    }

    suspend fun queryICO(): MutableMap<String,Int> {
        var ans: MutableMap<String,Int> = mutableMapOf<String,Int>()
        try{
            if(db != null){
                val task1 = db.collection("pages").whereEqualTo("useICO",true).get()
                val task2 = db.collection("pages").whereEqualTo("useICO",false).get()
                ans["ICO"] = task1.await().size()
                ans["non Ico"] = task2.await().size()
            }
        }catch(e: Exception){
            ans = mutableMapOf<String,Int>()
            Log.i(TAG,"There was an error querying ICO")
        }

        return ans
    }

    suspend fun getEvents(color: Int): HashMap<Int,SimpleEvent>{
        val ans = HashMap<Int,SimpleEvent>()
        if(db!=null){
            var task = db.collection("events").get()
            var docs = task.await()

            if(task.isSuccessful&&docs!=null){
                var eventDate: Int= 0
                for (d in docs){
                    eventDate=
                        (CalendarUtils.dateStringFromFormat(
                            locale = Locale("en", "US"),
                            date = Date(d.get("date") as String),
                            format = CalendarUtils.DB_DATE_FORMAT
                        )
                            ?: "0").toInt()
                    if(!ans.containsKey(eventDate)){
                        ans[eventDate]= SimpleEvent(
                            title = d.get("name") as String,
                            date= Date(d.get("date") as String),
                            description = d.get("description") as String,
                            timeEnd = d.get("timeEnd") as String,
                            timeStart = d.get("timeStart") as String,
                            color=color
                        )
                    }
                }

            }
        }
        return ans
    }


    suspend fun updateViews(userId: String)
    {
        if(db!=null)
        {
            val userRef = db.collection("users").document(userId);
           val task= userRef.update("views", FieldValue.increment(1))
            task.await()
        }
    }

    suspend fun getPageById(pageId: String): StartupPage
    {
        System.out.println("Id en firestore: " + pageId)
        var page: StartupPage= StartupPage()
        if (db != null) {
            val task = db.collection("pages").document(pageId).get()
            val document = task.await()
            if (task.isSuccessful && document != null) {
                System.out.println("Get page By id: " + document)

                page = StartupPage(
                    name = document.get("name") as String?,
                    homepage = document.get("homepage") as String?,
                    phoneNumber = document.get("phoneNumber") as String?,
                    email = document.get("email") as String?,
                    description = document.get("description") as String?,
                    country = document.get("country") as String?,
                    invest = document.get("invest") as String?,
                    preferredFinancial = document.get("preferredFinancial") as String?,
                    ownerUID = document.get("ownerUID") as String?,
                    profilePicUrl = document.get("profilePicUrl") as String?,
                    id = document.id
                )
            }
        }
        return page
    }


    suspend fun updateSuccesfulTwoFactor(){
        Log.i("2F","PEPETO")
        try {
            if (db != null) {
                val task = db.collection("2fa").document("traces")
                    .update("accepted", FieldValue.increment(1))
                task.await()
            }
        }catch (e:Exception){}
    }

    suspend fun updateUnSuccesfulTwoFactor(){
        try {
            if (db != null) {
                val task = db.collection("2fa").document("traces")
                    .update("rejected", FieldValue.increment(1))
                task.await()
            }
        }catch(e:Exception){}
    }

    suspend fun getUserName(uid: String):String? {
        var name: String? = null
        if (db != null) {
            val task = db.collection("users").document(uid).get()
            val document = task.await()
            if (task.isSuccessful && document != null) {
                name=document.get("firstName") as String? +" " + document.get("lastName") as String?
            }
        }
        return name
    }



}