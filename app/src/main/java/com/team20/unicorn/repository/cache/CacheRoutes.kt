package com.team20.unicorn.repository.cache

import android.util.Log
import android.util.LruCache
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.FileReader
import com.google.gson.stream.JsonReader
import com.team20.unicorn.UnicornApplication
import java.io.File

object CacheRoutes {
    var filePath: String = "LRUCache"
    private lateinit var LRUPhotos: LruCache<String, String>
    val cacheType = object : TypeToken<LruCache<String, String>>() {}.type

    init{
        initialize()
    }

    fun initialize() {
        filePath = File(UnicornApplication.applicationContext().cacheDir,filePath).toString()
        if (checkExistance(filePath)) {
            try {
                var jsonReader = JsonReader(FileReader(filePath))
                if (jsonReader != null)
                    LRUPhotos = Gson().fromJson<LruCache<String, String>>(jsonReader, cacheType)
            }catch(e: Exception){
                e.printStackTrace()
                LRUPhotos = LruCache<String, String>(((Runtime.getRuntime().maxMemory() / 10).toInt()))
            }
        }
        else{
            LRUPhotos = LruCache<String, String>(((Runtime.getRuntime().maxMemory() / 10).toInt()))
        }
    }

    fun checkExistance(check: String): Boolean{
        return File(check).exists()
    }

    fun getKey(key: String): String?{
        return LRUPhotos.get(key)?:""
    }

    fun deleteKey(key: String){
        LRUPhotos.remove(key)
    }

    fun setKey(key: String, value: String){
        LRUPhotos.put(key,value)
    }

    fun saveState(){
        val use = Gson().toJson(LRUPhotos, cacheType)
        File(filePath).writeText(use.toString())
    }
}
