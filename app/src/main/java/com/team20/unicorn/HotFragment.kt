package com.team20.unicorn

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.team20.unicorn.databinding.FragmentHotBinding
import com.team20.unicorn.viewModel.HotViewModel

class HotFragment : Fragment() {

    companion object {
        fun newInstance() = HotFragment()
    }

    private lateinit var viewModel: HotViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentHotBinding>(inflater,R.layout.fragment_hot,container,false)

        viewModel = ViewModelProvider(this).get(HotViewModel::class.java)


        viewModel.ico.observe(viewLifecycleOwner, Observer {
            updatePieChart(binding.icoProgressbar,it,"startups using ICO")
        })

        viewModel.fundingType.observe(viewLifecycleOwner, Observer {
            updatePieChart(binding.fundingProgressbar,it,"funding type")
        })

        return binding.root
    }

    fun updatePieChart(pieChart: PieChart, data: MutableMap<String,Int>, title: String){
        Log.i("PIECHART","it is coming here")
        val entries: ArrayList<PieEntry> = ArrayList<PieEntry>()
        var total = 0
        for((key,value) in data.entries){
            total += value
        }
        for((key,value) in data.entries){
            entries.add(PieEntry(value.toFloat()/total.toFloat(),key))
        }

        var colors: ArrayList<Int> = ArrayList<Int>()

        for(color in ColorTemplate.MATERIAL_COLORS){
            colors.add(color)
        }

        for(color in ColorTemplate.VORDIPLOM_COLORS){
            colors.add(color)
        }

        var dataSet = PieDataSet(entries,title)
        dataSet.setColors(colors)

        var data = PieData(dataSet)
        data.setDrawValues(true)
        data.setValueTextSize(12f)
        data.setValueTextColor(Color.BLACK)

        pieChart.data = data
        pieChart.invalidate()
    }
}