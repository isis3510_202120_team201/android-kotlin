package com.team20.unicorn

import android.content.DialogInterface
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.team20.unicorn.databinding.FragmentIndustriesBinding
import com.team20.unicorn.helper.ConnectionHelper
import com.team20.unicorn.helper.IndustriesAdapter
import com.team20.unicorn.viewModel.IndustriesViewModel

class IndustriesFragment : Fragment() {


    private lateinit var viewModel: IndustriesViewModel
    private  lateinit var industriesRecyclerView: RecyclerView
    private lateinit var binding : FragmentIndustriesBinding
    private val connectionHelper: ConnectionHelper by lazy {ConnectionHelper()}


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this).get(IndustriesViewModel::class.java)

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val activity = activity as MainActivity
        binding = DataBindingUtil.inflate<FragmentIndustriesBinding>(inflater,
            R.layout.fragment_industries,
            container,
            false)

        if (connectionHelper.checkForInternet(requireContext())) {
            binding.progressBarIndustries.isVisible = true
            viewModel.getData(requireContext())
            industriesRecyclerView = binding.industriesList
            industriesRecyclerView.layoutManager = LinearLayoutManager(activity)
            viewModel.industriesArrayList.observe(viewLifecycleOwner, Observer { newArrayList ->
                binding.progressBarIndustries.isVisible = false
                industriesRecyclerView.adapter = IndustriesAdapter(newArrayList)
            })
        }
        else
        {
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.apply {
                setTitle("Internet connection")
                setMessage("To use this feature you need Internet connection. Please navigate to other view.")
                setNeutralButton("Ok",
                    DialogInterface.OnClickListener { dialog, id ->
                        binding.noConnection.isVisible=true
                        binding.txtNoConn.isVisible=true
                    })
            }.create().show()
        }
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(IndustriesViewModel::class.java)
        // TODO: Use the ViewModel
    }

}