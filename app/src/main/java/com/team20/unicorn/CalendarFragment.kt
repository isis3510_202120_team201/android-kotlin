package com.team20.unicorn

import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.team20.unicorn.databinding.FragmentCalendarBinding
import com.team20.unicorn.helper.CalendarUtils
import com.team20.unicorn.helper.ConnectionHelper
import com.team20.unicorn.helper.RecyclerCalendarConfiguration
import com.team20.unicorn.helper.VerticalRecyclerCalendarAdapter
import com.team20.unicorn.model.SimpleEvent
import com.team20.unicorn.viewModel.CalendarViewModel
import com.team20.unicorn.viewModel.CalendarViewModelFactory
import java.util.*
import kotlin.collections.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CalendarFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CalendarFragment : Fragment() {

//    private val eventMap: HashMap<Int, SimpleEvent> = HashMap()
    private lateinit var eventMap: HashMap<Int, SimpleEvent>
    private lateinit var viewModel: CalendarViewModel
    private lateinit var viewModelFactory: CalendarViewModelFactory
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentCalendarBinding>(inflater,R.layout.fragment_calendar,container,false)

        val calendarRecyclerView: RecyclerView =binding.calendarRecyclerView

        viewModelFactory = CalendarViewModelFactory(R.color.DarkBlue)
        viewModel = ViewModelProvider(this,viewModelFactory)
            .get(CalendarViewModel::class.java)


        viewModel.eventsList.observe(viewLifecycleOwner, {events ->
            eventMap = events
            createCalendar(calendarRecyclerView)
        })
        viewModel.getEvents(R.color.DarkBlue)
        return binding.root
    }

    fun createCalendar(calendarRecyclerView: RecyclerView){
        val date = Date()
        date.time = System.currentTimeMillis()
        val startCal = Calendar.getInstance()
        val endCal = Calendar.getInstance()
        endCal.time = date
        endCal.add(Calendar.MONTH, 12)
        val configuration =
            RecyclerCalendarConfiguration(
                calenderViewType = RecyclerCalendarConfiguration.CalenderViewType.VERTICAL,
                calendarLocale = Locale("en", "US"),
                includeMonthHeader = true
            )

        configuration.weekStartOffset = RecyclerCalendarConfiguration.START_DAY_OF_WEEK.MONDAY

        val calendarAdapterVertical =
            VerticalRecyclerCalendarAdapter(
                startDate = startCal.time,
                endDate = endCal.time,
                configuration = configuration,
                eventMap = eventMap,
                dateSelectListener = object : VerticalRecyclerCalendarAdapter.OnDateSelected {
                    override fun onDateSelected(date: Date, event: SimpleEvent?) {
                        val selectedDate: String =
                            CalendarUtils.dateStringFromFormat(
                                locale = configuration.calendarLocale,
                                date = date,
                                format = CalendarUtils.LONG_DATE_FORMAT
                            )
                                ?: ""

                        if (event != null) {
                            AlertDialog.Builder(activity as MainActivity)
                                .setTitle(event.title)
                                .setMessage(
                                    String.format(
                                        Locale.getDefault(),
                                        "Date: %s\n\nDescription: %s\n\nStart Time: %s\n\nEnd Time: %s\n\n",
                                        selectedDate,
                                        event.description,
                                        event.timeStart,
                                        event.timeEnd
                                    )
                                )
                                .create()
                                .show()
                        }
                    }
                }
            );

        calendarRecyclerView.adapter = calendarAdapterVertical

        if (!connectionHelper.checkForInternet(requireContext())) {
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.apply {
                setTitle("Internet connection")
                setMessage("You don't have internet connection. We are showing cached info in your phone.")
                setNeutralButton("Ok",
                    DialogInterface.OnClickListener { dialog, id ->
                    })
            }.create().show()
        }
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CalendarFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CalendarFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}