package com.team20.unicorn

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.team20.unicorn.databinding.FragmentEditProfileBinding
import com.team20.unicorn.viewModel.*
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import android.app.Activity.RESULT_OK
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.provider.Settings
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import com.team20.unicorn.helper.ConnectionHelper
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [EditProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EditProfileFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var viewModel: ProfileViewModel
    private lateinit var viewModelFactory: ProfileViewModelFactory
    private lateinit var imgBackground: ImageView
    private lateinit var imgProfile: ImageView
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }


    private val GALLERY = 0
    private val CAMERA = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentEditProfileBinding>(inflater,R.layout.fragment_edit_profile,container,false)
        val activity = activity as MainActivity
        var id = ""

        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null) {
            id = sharedPref.getString("userId","").toString()
        }

        viewModelFactory = ProfileViewModelFactory(id)

        viewModel = ViewModelProvider(activity,viewModelFactory)
            .get(ProfileViewModel::class.java)

        viewModel.name.observe(viewLifecycleOwner, { firstname ->
            binding.editFirstName.hint = firstname
        })
        viewModel.lastname.observe(viewLifecycleOwner, { lastname ->
            binding.editLastname.hint = lastname
        })
        viewModel.linkedInProf.observe(viewLifecycleOwner, { prof ->
            binding.editLinkedIn.hint = prof
        })
        imgBackground = binding.imgBackgroundEdit
        imgProfile = binding.imgChangeProfile
        binding.bttnConfirm.setOnClickListener{

                val builder = AlertDialog.Builder(activity)
                builder.setMessage("Are you sure you want to confirm your changes?")
                    .setCancelable(true)
                    .setPositiveButton("Yes") { dialog, id ->
                        var newFirstName = binding.editFirstName.text.toString()
                        var newLastName = binding.editLastname.text.toString()
                        var newLinkedInProfile = binding.editLinkedIn.text.toString()
                        if (newFirstName.isEmpty() && newLastName.isEmpty() && newLinkedInProfile.isEmpty() && viewModel.bannerUri == Uri.EMPTY && viewModel.profileUri == Uri.EMPTY && viewModel.bannerBmp == null && viewModel.profileBmp == null) {
                            dialog.dismiss()
                            Toast.makeText(
                                activity as Activity,
                                "No changes detected, canceling update",
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            if (binding.editFirstName.text.toString().isEmpty()) {
                                newFirstName = viewModel.name.value.toString()
                            }
                            if (binding.editLastname.text.toString().isEmpty()) {
                                newLastName = viewModel.lastname.value.toString()
                            }
                            if (binding.editLinkedIn.text.toString().isEmpty()) {
                                newLinkedInProfile = viewModel.linkedInProf.value.toString()
                            }
                            Log.i(
                                "GAL",
                                "Calling update profile wtith FN: $newFirstName LN: $newLastName linkedInProf: $newLinkedInProfile"
                            )
                            var bannerPic = viewModel.bannerPic.value?.first?:""
                            var profilePic = viewModel.profilePic.value?.first?:""

                            if(viewModel.bannerUri != Uri.EMPTY) bannerPic = viewModel.bannerUri.toString()
                            else if(viewModel.bannerBmp != null){
                                bannerPic =
                                    viewModel.savePhotoToExternalStorage(System.currentTimeMillis().toString(),
                                        viewModel.bannerBmp!!,activity)
                                        .toString()
                            }

                            if(viewModel.profileUri != Uri.EMPTY) profilePic = viewModel.profileUri.toString()
                            else if(viewModel.profileBmp != null){
                                profilePic =
                                    viewModel.savePhotoToExternalStorage(System.currentTimeMillis().toString(),
                                        viewModel.profileBmp!!,activity)
                                        .toString()
                            }

                            viewModel.updateProfile(newFirstName, newLastName, newLinkedInProfile, profilePic, bannerPic)
                            requireView().findNavController()
                                .navigate(R.id.action_editProfileFragment_to_profileFragment)
                        }
                    }
                    .setNegativeButton("No") { dialog, id ->
                        // Dismiss the dialog
                        dialog.dismiss()
                    }
                val alert = builder.create()
                alert.show()
            if(!connectionHelper.checkForInternet(requireContext())) {
                val alertDialog = androidx.appcompat.app.AlertDialog.Builder(requireContext())
                alertDialog.apply {
                    setTitle("Internet connection")
                    setMessage("You don't have internet connetion, your changes will be upload when you hace internet again")
                    setNeutralButton("Ok",
                        DialogInterface.OnClickListener { dialog, id ->
                        })
                }.create().show()
            }
        }

        binding.bttnCancel.setOnClickListener{
            val builder = AlertDialog.Builder(activity)
            builder.setMessage("Are you sure you want to cancel editing your profile?")
                .setCancelable(true)
                .setPositiveButton("Yes") { dialog, id ->

                    requireView().findNavController()
                            .navigate(EditProfileFragmentDirections.actionEditProfileFragmentToProfileFragment())

                }
                .setNegativeButton("No") { dialog, id ->
                    // Dismiss the dialog
                    dialog.dismiss()
                }
            val alert = builder.create()
            alert.show()
        }
        binding.imgBackgroundEdit.setOnClickListener{
            viewModel.caller = true
            selectImageDialog(true)
        }
        binding.imgChangeProfile.setOnClickListener{
            viewModel.caller = false
            selectImageDialog(false)
        }
        return binding.root
    }

    fun selectImageDialog(caller: Boolean) {

        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Add Photo!")
        val options = arrayListOf("Choose from Gallery", "Take Photo", "Cancel")
        val exampleItems: Array<CharSequence?>? =
            options.toList()?.size?.let { it1 -> Array(it1, { i -> options?.get(i) }) }
        builder.setItems(exampleItems) { dialog, which ->
            when (which) {
                0 -> {
                    if (askForGalleryPermissions()) {
                        if (caller) {
                            selectImageFromGallery(1)
                        } else {
                            selectImageFromGallery(3)
                        }
                    }
                }
                1 -> {
                    if (askForCameraPermissions()) {
                        if(caller){
                            takePhoto(2)
                        }
                        else{
                            takePhoto(4)
                        }
                    }
                }
                else -> {
                    dialog.dismiss()
                }
            }
        }
        builder.show()
    }

    fun takePhoto(code: Int){
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, code)
    }

    fun selectImageFromGallery(code: Int){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, "image/jpeg")
        startActivityForResult(intent, code)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == 1){
            val bitmap = MediaStore.Images.Media.getBitmap((activity as MainActivity).contentResolver, data?.data)
            val prueba = data?.data?.toString()?:"No"
            val newBmp = viewModel.resizeBitmap(bitmap,300)
            val jpegUri = bitmapToFile(newBmp)
            viewModel.bannerUri = prueba?.toUri()!!
            imgBackground.setImageBitmap(newBmp)
        }
        else if (resultCode == RESULT_OK && requestCode == 3){
            val bitmap = MediaStore.Images.Media.getBitmap((activity as MainActivity).contentResolver, data?.data)
            val prueba = data?.data?.toString()?:"No"
            val newBmp = viewModel.resizeBitmap(bitmap,300)
            viewModel.profileUri = prueba?.toUri()!!
            imgProfile.setImageBitmap(newBmp)
        }
        else if (resultCode == RESULT_OK && requestCode == 2 && data != null){
            val bitmap = data.extras?.get("data") as Bitmap
            val newBmp = viewModel.resizeBitmap(bitmap,300)
            viewModel.bannerBmp = newBmp
            imgBackground.setImageBitmap(newBmp)
        }
        else if (resultCode == RESULT_OK && requestCode == 4 && data != null){
            val bitmap = data.extras?.get("data") as Bitmap
            val newBmp = viewModel.resizeBitmap(bitmap,300)
            imgProfile.setImageBitmap(newBmp)
            viewModel.profileBmp = newBmp
        }
    }


    // Method to save an bitmap to a file/NetworkRequest: No
    private fun bitmapToFile(bitmap:Bitmap): Uri {
        // Get the context wrapper
        val wrapper = ContextWrapper(activity as MainActivity)

        // Initialize a new file instance to save bitmap object
        var file = wrapper.getDir("Images",Context.MODE_PRIVATE)
        file = File(file,"${UUID.randomUUID()}.jpeg")

        try{
            // Compress the bitmap and save in jpg format
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream)
            stream.flush()
            stream.close()
        }catch (e: IOException){
            e.printStackTrace()
        }

        // Return the saved bitmap uri
        return Uri.parse(file.absolutePath)
    }
    fun isGalleryPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(activity as MainActivity,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }
    fun isCameraPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(activity as MainActivity,Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    fun askForCameraPermissions(): Boolean {

        if (!isCameraPermissionsAllowed()) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                showPermissionDeniedDialog(CAMERA)
                //Toast.makeText(activity,"Please authorize the app to access to Gallery",Toast.LENGTH_LONG).show()
            } else {
                requestPermissions(arrayOf(Manifest.permission.CAMERA),CAMERA)
            }
            return false
        }
        return true
    }

    fun askForGalleryPermissions(): Boolean {
        if (!isGalleryPermissionsAllowed()) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showPermissionDeniedDialog(GALLERY)
                //Toast.makeText(activity,"Please authorize the app to access to Gallery",Toast.LENGTH_LONG).show()
            } else {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),GALLERY)
            }
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<String>,grantResults: IntArray) {
        when (requestCode) {
            GALLERY -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(viewModel.caller){
                        selectImageFromGallery(1)
                    }
                    else{
                        selectImageFromGallery(3)

                    }
                } else {
                    showPermissionDeniedDialog(GALLERY)
                    // permission is denied, you can ask for permission again, if you want
                    //  askForPermissions()
                }
                return
            }
            CAMERA -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(viewModel.caller){
                        takePhoto(2)
                    }
                    else{
                        takePhoto(4)

                    }
                } else {
                    showPermissionDeniedDialog(CAMERA)
                    // permission is denied, you can ask for permission again, if you want
                    //  askForPermissions()
                }
                return
            }
        }
    }

    private fun showPermissionDeniedDialog(permission: Int) {
        var message = "Permission to access Gallery is denied. Please allow permissions from App Settings to select an image from your phone's gallery."
        if(permission != GALLERY){
            message = "Permission to access Camera is denied. Please allow permissions from App Settings to take a photo with your phone's camera."
        }
        AlertDialog.Builder(activity)
            .setTitle("Permission Denied")
            .setMessage(message)
            .setPositiveButton("App Settings",
                DialogInterface.OnClickListener { dialogInterface, i ->
                    // send to app settings if permission is denied permanently
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package", activity?.packageName, null)
                    intent.data = uri
                    startActivity(intent)
                })
            .setNegativeButton("Cancel",null)
            .show()
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment EditProfileFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            EditProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}