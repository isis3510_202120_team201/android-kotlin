package com.team20.unicorn

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.team20.unicorn.databinding.FragmentSignInBinding
import com.team20.unicorn.viewModel.SignInViewModel
import android.text.TextUtils
import android.util.Patterns
import androidx.appcompat.app.AlertDialog
import com.team20.unicorn.helper.ConnectionHelper


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SignInFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SignInFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var backButton : ImageButton
    private lateinit var inputEmail: EditText
    private lateinit var password: EditText
    private lateinit var signInButton: Button
    private lateinit var viewModel: SignInViewModel
    private lateinit var forgotPasswdButton: Button
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentSignInBinding>(inflater,R.layout.fragment_sign_in,container,false)
        binding.backButton.setOnClickListener{ view: View ->
            view.findNavController().navigate(R.id.action_signInFragment_to_welcomeFragment2)
        }

        viewModel = ViewModelProvider(this).get(SignInViewModel::class.java)

        viewModel.success.observe(viewLifecycleOwner, Observer { newSucess ->
            handleSuccess(newSucess)
        })

        inputEmail = binding.inputEmail
        inputEmail.addTextChangedListener(textWatcher)

        password = binding.inputPassword
        password.addTextChangedListener(textWatcher)

        signInButton = binding.singInButton
        signInButton.setOnClickListener{ view: View-> signIn() }
        signInButton.isEnabled = false

        viewModel.dialogMessage.observe(viewLifecycleOwner, Observer { message ->
            showDialog("Failed to sing in", message)
        })

        forgotPasswdButton = binding.forgotPasswordButton
        forgotPasswdButton.setOnClickListener{forgotPassword()}
        return binding.root
    }

    private fun handleSuccess(newSuccess: Boolean){
        if(newSuccess){

                val activity = activity as MainActivity
                val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
                if (sharedPref != null) {
                    with(sharedPref.edit()) {
                        putString("userId", viewModel.userId)
                        apply()
                    }
                }
                Toast.makeText(activity as Activity, "Success", Toast.LENGTH_SHORT).show()

                if(!viewModel.twoFactor)
                    requireView().findNavController().navigate(R.id.action_signInFragment_to_mainFragment)
                else
                    requireView().findNavController().navigate(SignInFragmentDirections.actionSignInFragmentToTwoFactorFragment(viewModel.phoneNumber?:"",false))
        }
        else{
            Toast.makeText(activity as Activity, "Authentication failed.",
                Toast.LENGTH_SHORT).show()
        }
    }

    private fun signIn()
    {
        if(connectionHelper.checkForInternet(requireContext())) {
            val email = inputEmail.text.toString()
            if (isValidEmail(email)) {
                val password = password.text.toString()
                val myTrace = Firebase.performance.newTrace("signin_trace")
                myTrace.start()
                viewModel.signIn(email, password)
                myTrace.stop()
            } else {
                showDialog("Failed to sing in", "The email doesn't have a valid format")
            }
        }
        else
        {
            showDialog("Internet connection", "To sing in Unicorn App you need internet connection")
        }
    }

    private fun forgotPassword ()
    {
        showDialog("Feature doesn't available", "This feature will be available in the future. We are working for you")
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if( inputEmail.text.toString().length==0 ||
                password.text.toString().length<5){
                signInButton.isEnabled = false
            }
            else{
                signInButton.isEnabled = true
            }
        }
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SignInFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SignInFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
    

    fun showDialog (title:String, message: String)
    {
        val alertDialog = AlertDialog.Builder(requireContext())
        alertDialog.apply {
            setTitle(title)
            setMessage(message)
            setNeutralButton("Ok",
                DialogInterface.OnClickListener { dialog, id ->

                })
        }.create().show()
    }
}