package com.team20.unicorn

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import android.view.inputmethod.InputMethodManager
import androidx.core.view.isVisible
import com.team20.unicorn.databinding.FragmentSearchUserBinding
import com.team20.unicorn.viewModel.SearchUserModelFactory
import com.team20.unicorn.viewModel.SearchUserViewModel
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SearchUserFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var usersRecyclerView : RecyclerView
    private lateinit var binding : FragmentSearchUserBinding
    private lateinit var viewModel: SearchUserViewModel
    private lateinit var viewModelFactory: SearchUserModelFactory


    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)

        }
        val activity = activity as MainActivity
        val view=activity.currentFocus
        if (view!=null)
        {
            val imm=activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentSearchUserBinding>(inflater,R.layout.fragment_search_user,container,false)
        binding.progressBar2.isVisible=true
        var id = ""
        val activity = activity as MainActivity
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null) {
            id = sharedPref.getString("userId","").toString();
        }

        viewModelFactory = SearchUserModelFactory(id)

        viewModel = ViewModelProvider(this,viewModelFactory)
            .get(SearchUserViewModel::class.java)

        usersRecyclerView = binding.searchList
        usersRecyclerView.layoutManager = LinearLayoutManager(activity)
        usersRecyclerView.setHasFixedSize(true)

        viewModel.usersArrayList.observe(viewLifecycleOwner, Observer { newUserArrayList ->
            binding.progressBar2.isVisible=false
            usersRecyclerView.adapter = viewModel.getAdapter(newUserArrayList)
        })

        viewModel.toGoUser.observe(viewLifecycleOwner, Observer { toGoUserId ->
            swapToProfile(toGoUserId)
        })

        return binding.root
    }


    fun swapToProfile(toGoUserId: String){
        val activity = activity as MainActivity
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if (sharedPref != null) {
            with(sharedPref.edit()) {
                putString("tempId", toGoUserId)
                apply()
            }
        }
        requireView().findNavController().navigate(SearchUserFragmentDirections.actionSearchUserFragmentToProfileFragment())
    }

}
