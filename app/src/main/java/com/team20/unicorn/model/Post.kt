package com.team20.unicorn.model

import com.google.firebase.Timestamp

data class Post( val uid: String? = null, val owner: String? = null, val datePosted: Timestamp? = null, val content : String? = null,val imgUrl: String? = null)

