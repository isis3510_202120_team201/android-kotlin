package com.team20.unicorn.model

data class StartupPage(
    val id: String = "",
    val name: String? = null,
    val homepage: String? = null,
    val phoneNumber: String? = null,
    val email: String? = null,
    val description: String? = null,
    val country: String? = null,
    val invest: String? = null,
    val preferredFinancial: String? = null,
    val ownerUID: String?=null,
    val profilePicUrl: String?=null,
    val framework: String? = null

)

