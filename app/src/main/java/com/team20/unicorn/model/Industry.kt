package com.team20.unicorn.model

data class Industry (val name: String? = null, val funding: Long? = null, val startups :Int? = null)
