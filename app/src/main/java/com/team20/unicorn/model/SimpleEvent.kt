package com.team20.unicorn.model

import java.util.*

class SimpleEvent(val date: Date, val title: String, val description: String, val timeEnd: String, val timeStart: String, val color: Int)