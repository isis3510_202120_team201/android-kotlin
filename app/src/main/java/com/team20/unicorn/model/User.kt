package com.team20.unicorn.model

data class User(val id: String? = null, val firstName: String? = null, val lastName: String? = null, val email: String? = null, val type : String? = null, val interests: MutableList<String>?=null, val survey: Int =0, val linkedInProfile: String? =null,val bannerPicUrl: String? = null,
                val profilePicUrl: String? = null,
                val phoneNumber : String? = null,
                val twoFactor: Boolean? = null)
