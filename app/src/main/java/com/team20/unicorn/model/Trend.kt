package com.team20.unicorn.model

data class Trend (val name: String? = null, val funding: Long? = null, val startups :Int? = null)
