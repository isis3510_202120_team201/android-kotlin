package com.team20.unicorn

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.team20.unicorn.databinding.FragmentPagesBinding
import androidx.navigation.findNavController
import com.team20.unicorn.helper.ConnectionHelper
import com.team20.unicorn.model.StartupPage
import com.team20.unicorn.viewModel.PagesViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


/**
 * A simple [Fragment] subclass.
 * Use the [PagesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PagesFragment : Fragment() {


    private lateinit var radioGroup: RadioGroup
    private lateinit var inputInvest: RadioButton
    private lateinit var createButton: Button
    private lateinit var inputName: EditText
    private lateinit var inputHomepage: EditText
    private lateinit var inputPhoneNumber: EditText
    private lateinit var inputEmail: EditText
    private lateinit var inputDescription: EditText
    private lateinit var inputCountry: EditText
    private lateinit var inputPreference: AutoCompleteTextView
    private lateinit var inputFramework: AutoCompleteTextView

    private lateinit var viewModel: PagesViewModel
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding=DataBindingUtil.inflate<FragmentPagesBinding>(inflater, R.layout.fragment_pages,container,false)
        viewModel = ViewModelProvider(this).get(PagesViewModel::class.java)
        val activity = activity as MainActivity
        if(connectionHelper.checkForInternet(requireContext())) {
            val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
            if (sharedPref != null) {
                viewModel.userId = sharedPref.getString("userId", "").toString();
            }

            inputName = binding.inputName
            inputName.addTextChangedListener(textWatcher)

            inputHomepage = binding.inputHomepage
            inputHomepage.addTextChangedListener(textWatcher)

            inputPhoneNumber = binding.inputPhoneNumber
            inputPhoneNumber.addTextChangedListener(textWatcher)

            inputEmail = binding.inputEmail
            inputEmail.addTextChangedListener(textWatcher)

            inputDescription = binding.inputDescription
            inputDescription.addTextChangedListener(textWatcher)

            inputCountry = binding.inputCountry
            inputCountry.addTextChangedListener(textWatcher)

            radioGroup = binding.radioGroupIFounding
            inputPreference = binding.autoCompleteTextView
            inputFramework = binding.autoCompleteStartups

            binding.backButtonProfile.setOnClickListener { view: View ->
                view.findNavController().navigate(R.id.action_pagesFragment_to_genericDashboardFragment)
            }

        viewModel.successCreate.observe(viewLifecycleOwner, Observer { res ->
            if(res) {
                Toast.makeText(activity as MainActivity, "Success creating page!", Toast.LENGTH_LONG).show()

            }
        })

            val foundingTypes = listOf(
                "Bank funding",
                "Business angel",
                "Venture capital",
                "Government funding",
                "Crowdfunding"
            )
            val arrayAdapter = ArrayAdapter(requireContext(), R.layout.list_item, foundingTypes)
            binding.autoCompleteTextView.setAdapter(arrayAdapter)
            val frameworks = listOf(
                "React",
                "Angular",
                "Vue"
            )
            val framewArrayAdapter = ArrayAdapter(requireContext(), R.layout.list_item, frameworks)
            binding.autoCompleteStartups.setAdapter(framewArrayAdapter)
        createButton=binding.createButton
        createButton.setOnClickListener{create() }
        createButton.isEnabled = false

        }
        else
        {
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.apply {
                setTitle("Internet connection")
                setMessage("To use this feature you need internet connection")
                setNeutralButton("Ok",
                    DialogInterface.OnClickListener { dialog, id ->
//                        binding.noConnection.isVisible=true
                    })
            }.create().show()
        }
        return binding.root
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (inputName.text.toString().length == 0 ||
                inputHomepage.text.toString().length == 0 ||
                inputPhoneNumber.text.toString().length == 0 ||
                inputEmail.text.toString().length == 0 ||
                inputDescription.text.toString().length == 0
            ) {
                createButton.isEnabled = false
            } else {
                createButton.isEnabled = true
            }
        }
    }

    private fun create ()
    {
        if(connectionHelper.checkForInternet(requireContext()))
        {
        val activity = activity as MainActivity
        val name=inputName.text.toString()
        val homePage=inputHomepage.text.toString()
        val phoneNumber=inputPhoneNumber.text.toString()
        val email=inputEmail.text.toString()
        val description=inputDescription.text.toString()
        val country = inputCountry.text.toString()
        var invest=""
        val preferences=inputPreference.text.toString()
        val selectedRadioButtonId: Int = radioGroup.checkedRadioButtonId
        val framework = inputFramework.text.toString()
        var id = ""
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if (selectedRadioButtonId != -1) {
            inputInvest= requireView().findViewById(selectedRadioButtonId)
            invest = inputInvest.text.toString()

        } else {

        }

        if(sharedPref != null && id == "") {
           id = sharedPref.getString("userId","").toString()
        }
        viewModel.addPage(
            StartupPage(

                name=name,
                homepage=homePage,
                phoneNumber=phoneNumber,
                email=email,
                description=description,
                country=country,
                invest=invest,
                preferredFinancial=preferences,
                ownerUID = id,
                framework=framework))
        }
        else
        {
            val alertDialog = androidx.appcompat.app.AlertDialog.Builder(requireContext())
            alertDialog.apply {
                setTitle("Internet connection")
                setMessage("To create a new page you need internet connection")
                setNeutralButton("Ok",
                    DialogInterface.OnClickListener { dialog, id ->
                    })
            }.create().show()
        }
    }

}