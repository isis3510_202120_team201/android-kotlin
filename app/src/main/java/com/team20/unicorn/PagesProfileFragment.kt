package com.team20.unicorn

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.team20.unicorn.databinding.FragmentPagesProfileBinding
import com.team20.unicorn.helper.ConnectionHelper
import com.team20.unicorn.viewModel.PagesViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


/**
 * A simple [Fragment] subclass.
 * Use the [PagesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PagesProfileFragment : Fragment() {

    private lateinit var name: TextView
    private lateinit var homepage: TextView
    private lateinit var phoneNumber:TextView
    private lateinit var email: TextView
    private lateinit var description: TextView
    private lateinit var country: TextView
    private lateinit var preference: TextView
    private lateinit var framework: TextView

    private lateinit var viewModel: PagesViewModel
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding=DataBindingUtil.inflate<FragmentPagesProfileBinding>(inflater, R.layout.fragment_pages_profile,container,false)
        viewModel = ViewModelProvider(this).get(PagesViewModel::class.java)
        val activity = activity as MainActivity

            val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
            if (sharedPref != null) {
                viewModel.userId = sharedPref.getString("userId", "").toString();
            }

            name = binding.EntrepName
            homepage = binding.inputHomepage
            phoneNumber = binding.inputPhoneNumber
            email = binding.inputEmail
            description = binding.inputDescription
            country = binding.inputCountry
            preference = binding.preferredFounding
            framework = binding.preferredFramwork

            binding.backButtonProfile.setOnClickListener { view: View ->
                view.findNavController().navigate(R.id.action_pagesProfileFragment_to_genericDashboardFragment)
            }

            viewModel.getPageById(PagesProfileFragmentArgs.fromBundle(requireArguments()).id)


            viewModel.page.observe(viewLifecycleOwner, { pag ->
                pag.ownerUID?.let { viewModel.updateViews(it) }
                System.out.println(pag)
                binding.EntrepName.setText(pag.name)
                binding.inputHomepage.setText(pag.homepage)
                binding.inputPhoneNumber.setText(pag.phoneNumber)
                binding.inputEmail.setText(pag.email)
                binding.inputDescription.setText(pag.description)
                binding.inputCountry.setText(pag.country)
                binding.preferredFounding.setText(pag.preferredFinancial)
                binding.preferredFramwork.setText(pag.framework)
            })

        if(!connectionHelper.checkForInternet(requireContext())) {
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.apply {
                setTitle("Internet connection")
                setMessage("You don't have internet connection. We're showing which we cant download the last time you was online")
                setNeutralButton("Ok",
                    DialogInterface.OnClickListener { dialog, id ->
//                        binding.noConnection.isVisible=true
                    })
            }.create().show()
        }
        return binding.root
    }




}