package com.team20.unicorn

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.google.android.material.tabs.TabLayout
import com.team20.unicorn.databinding.FragmentTrendsIndustriesBinding
import com.team20.unicorn.viewModel.ProfileViewModelFactory
import com.team20.unicorn.viewModel.TrendsViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TrendsIndustriesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TrendsIndustriesFragment : Fragment() {

    private var actual : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentTrendsIndustriesBinding>(inflater,R.layout.fragment_trends_industries,container,false)
        actual = "INDUS"

        var manager = childFragmentManager
        var see = manager.fragments[0]
        var controller = see.requireView().findNavController()

        binding.profileTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when(tab?.position){
                    0 -> {
                        when(actual){
                            "TRENDS" -> controller.navigate(R.id.action_trendsFragment_to_industriesFragment2)
                            else -> {}
                        }
                        actual = "INDUS"
                    }
                    1 -> {
                        when(actual){
                            "INDUS" -> controller.navigate(R.id.action_industriesFragment_to_trendsFragment2)
                            else -> {}
                        }
                        actual = "TRENDS"
                    }

                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                // Handle tab reselect
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                // Handle tab unselect
            }
        })
        return binding.root
    }

}