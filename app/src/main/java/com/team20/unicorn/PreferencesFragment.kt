package com.team20.unicorn

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.team20.unicorn.databinding.FragmentPreferencesBinding
import com.team20.unicorn.viewModel.PreferencesViewModel
import com.team20.unicorn.viewModel.PreferencesViewModelFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PreferencesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PreferencesFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var viewModel: PreferencesViewModel
    private lateinit var buttonBack: ImageButton
    private lateinit var buttonCrypto : Button
    private lateinit var buttonFintech : Button
    private lateinit var buttonInnovation : Button
    private lateinit var buttonBlockchain : Button
    private lateinit var buttonRandom : Button
    private lateinit var buttonStart : Button
    private lateinit var userInts: MutableMap<String,Boolean>
    private lateinit var viewModelFactory: PreferencesViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentPreferencesBinding>(inflater,R.layout.fragment_preferences,container,false)

        val activity = activity as MainActivity
        var uid = ""
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null) {
            uid = sharedPref.getString("userId","").toString()
        }
        viewModelFactory = PreferencesViewModelFactory(uid)

        viewModel = ViewModelProvider(this,viewModelFactory)
            .get(PreferencesViewModel::class.java)

        viewModel.success.observe(viewLifecycleOwner, Observer { newSucess ->
            handleSuccess(newSucess)
        })

        binding.lifecycleOwner = viewLifecycleOwner
        userInts = viewModel.userInts

        buttonBack = binding.backButton
        buttonCrypto = binding.bttnCrypto
        buttonFintech = binding.bttnFintech
        buttonBlockchain = binding.bttnBlockchain
        buttonInnovation = binding.bttnInnovation
        buttonRandom = binding.bttnRandom
        buttonStart = binding.bttnStart

        buttonStart.isEnabled = false

        buttonCrypto.setOnClickListener {
            onClick(buttonCrypto)
        }
        buttonFintech.setOnClickListener {
            onClick(buttonFintech)
        }
        buttonBlockchain.setOnClickListener {
            onClick(buttonBlockchain)
        }
        buttonInnovation.setOnClickListener {
            onClick(buttonInnovation)
        }
        buttonRandom.setOnClickListener {
            onClick(buttonRandom)
        }
        buttonStart.setOnClickListener {
            onClick(buttonStart)
        }
        buttonBack.setOnClickListener{
            onClick(buttonBack)
        }
        return binding.root
    }

    fun handleSuccess(newSuccess: Boolean){
        if(newSuccess) {
            Toast.makeText(activity as Activity, "Success", Toast.LENGTH_SHORT).show()
            requireView().findNavController()
                .navigate(R.id.action_preferencesFragment_to_mainFragment)
        }
        else{
            Toast.makeText(activity as Activity, "Failed database", Toast.LENGTH_LONG).show()
        }
    }

    fun onClick(v: View){
        when(v.id){
            buttonCrypto.id -> {
                if(userInts["Crypto"] == false){
                    buttonCrypto.setBackgroundColor(resources.getColor(R.color.Lighter))
                    buttonCrypto.setTextColor(resources.getColor(R.color.black))
                    userInts["Crypto"] = true
                    buttonStart.isEnabled = userInts.containsValue(true)
                }
                else{
                    buttonCrypto.setBackgroundColor(resources.getColor(R.color.Darker))
                    buttonCrypto.setTextColor(resources.getColor(R.color.white))
                    userInts["Crypto"] = false
                    buttonStart.isEnabled = userInts.containsValue(true)
                }
            }
            buttonBlockchain.id -> {
                if(userInts["Blockchain"] == false){
                    buttonBlockchain.setBackgroundColor(resources.getColor(R.color.Lighter))
                    buttonBlockchain.setTextColor(resources.getColor(R.color.black))
                    userInts["Blockchain"] = true
                    buttonStart.isEnabled = userInts.containsValue(true)
                }
                else{
                    buttonBlockchain.setBackgroundColor(resources.getColor(R.color.Darker))
                    buttonBlockchain.setTextColor(resources.getColor(R.color.white))
                    userInts["Blockchain"] = false
                    buttonStart.isEnabled = userInts.containsValue(true)
                }
            }
            buttonFintech.id -> {
                if(userInts["Fintech"] == false){
                    buttonFintech.setBackgroundColor(resources.getColor(R.color.Lighter))
                    buttonFintech.setTextColor(resources.getColor(R.color.black))
                    userInts["Fintech"] = true
                    buttonStart.isEnabled = userInts.containsValue(true)
                }
                else{
                    buttonFintech.setBackgroundColor(resources.getColor(R.color.Darker))
                    buttonFintech.setTextColor(resources.getColor(R.color.white))
                    userInts["Fintech"] = false
                    buttonStart.isEnabled = userInts.containsValue(true)
                }

            }
            buttonInnovation.id -> {
                if(userInts["Innovation"] == false){
                    buttonInnovation.setBackgroundColor(resources.getColor(R.color.Lighter))
                    buttonInnovation.setTextColor(resources.getColor(R.color.black))
                    userInts["Innovation"] = true
                    buttonStart.isEnabled = userInts.containsValue(true)
                }
                else{
                    buttonInnovation.setBackgroundColor(resources.getColor(R.color.Darker))
                    buttonInnovation.setTextColor(resources.getColor(R.color.white))
                    userInts["Innovation"] = false
                    buttonStart.isEnabled = userInts.containsValue(true)
                }
            }
            buttonRandom.id -> {
                if(userInts["Random"] == false){
                    buttonRandom.setBackgroundColor(resources.getColor(R.color.Lighter))
                    buttonRandom.setTextColor(resources.getColor(R.color.black))
                    userInts["Random"] = true
                    buttonStart.isEnabled = userInts.containsValue(true)
                }
                else{
                    buttonRandom.setBackgroundColor(resources.getColor(R.color.Darker))
                    buttonRandom.setTextColor(resources.getColor(R.color.white))
                    userInts["Random"] = false
                    buttonStart.isEnabled = userInts.containsValue(true)
                }
            }
            buttonStart.id -> {

                var userIntsDef = mutableListOf<String>()
                userInts.forEach { (interes, valor) ->
                    if (userInts[interes] == true){
                        userIntsDef.add(interes)
                    }
                }
                viewModel.addInterests(userIntsDef)
            }
            buttonBack.id -> {
                v.findNavController().navigate(R.id.action_preferencesFragment_to_chooseRoleFragment)
            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PreferencesFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PreferencesFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}