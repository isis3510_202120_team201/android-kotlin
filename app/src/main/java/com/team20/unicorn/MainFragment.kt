package com.team20.unicorn

import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.team20.unicorn.databinding.FragmentMainBinding
import com.team20.unicorn.helper.ConnectionHelper

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MainFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MainFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding : FragmentMainBinding
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentMainBinding>(inflater,R.layout.fragment_main,container,false)

        binding.fab.setOnClickListener{

                requireView().findNavController().navigate(R.id.action_mainFragment_to_postFragment)

            if(!connectionHelper.checkForInternet(requireContext())) {
                val alertDialog = AlertDialog.Builder(requireContext())
                alertDialog.apply {
                    setTitle("Internet connection")
                    setMessage("You don't have internet connection. You new post will be upload when you have internet connection again")
                    setNeutralButton("Ok",
                        DialogInterface.OnClickListener { dialog, id ->

                        })
                }.create().show()
            }
        }

        return binding.root
    }


    fun logOut(){
        var auth: FirebaseAuth = Firebase.auth
        auth.signOut()
        requireView().findNavController().navigate(R.id.action_mainFragment_to_welcomeFragment2)
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MainFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MainFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

}