package com.team20.unicorn

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.team20.unicorn.databinding.FragmentInformationBinding
import com.team20.unicorn.viewModel.ProfileViewModel
import com.team20.unicorn.viewModel.ProfileViewModelFactory
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [InformationFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class InformationFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var viewModel: ProfileViewModel
    private lateinit var viewModelFactory: ProfileViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentInformationBinding>(inflater,R.layout.fragment_information,container,false)
        val activity = activity as MainActivity
        var id = ""

        var see = parentFragment

        when(see){
            is NavHostFragment -> see = see.parentFragment
        }

        when(see){
            is ProfileFragment -> see = see as ProfileFragment
        }

        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null && sharedPref.getString("tempId","") != "") {
            id = sharedPref.getString("tempId","").toString()
        }

        if(sharedPref != null && id == "") {
            id = sharedPref.getString("userId","").toString()
        }

        val myTrace = Firebase.performance.newTrace("linkedin_trace")

        viewModelFactory = ProfileViewModelFactory(id)

        val txtLinkedId: TextView =binding.txtLinkedInProfile
        txtLinkedId.setOnClickListener(View.OnClickListener {

            myTrace.start()

            // code that you want to trace (and log custom metrics)
            val intent = Intent(Intent.ACTION_VIEW)

            myTrace.stop()
        })


        if(see != null)
            viewModel = ViewModelProvider(see,viewModelFactory)
                .get(ProfileViewModel::class.java)

        viewModel.email.observe(viewLifecycleOwner, { email ->
            binding.txtEmailProfile.text = email
        })
        viewModel.linkedInProf.observe(viewLifecycleOwner, {prof ->
            txtLinkedId.text = prof
        })
        viewModel.userType.observe(viewLifecycleOwner, {type ->
            myTrace.putAttribute("user_type", type)
        })

        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment InformationFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            InformationFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}