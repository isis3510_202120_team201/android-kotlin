package com.team20.unicorn.helper

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.team20.unicorn.R
import com.team20.unicorn.model.Post
import com.team20.unicorn.repository.database.FirestoreService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MyAdapter(private val postList: ArrayList<Post>): RecyclerView.Adapter<MyAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item,parent,false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val currentItem = postList[position]
        var name =currentItem.owner
         MainScope().launch(Dispatchers.IO) {

            val task = async { FirestoreService.getUserName(name.toString())}
            name = task.await()
             MainScope().launch(Dispatchers.Main) {
                 holder.postName.text =name
             }

        }


        holder.postContent.text = currentItem.content
        holder.postDate.text = currentItem.datePosted?.toDate().toString()

        MainScope().launch {
            var url = "posts/${currentItem.owner}/${currentItem.uid}/post.jpeg"
            var use = FirestoreService.getImage(url,currentItem.uid?:"")
            var myTrace = Firebase.performance.newTrace("get_image")
            myTrace.start()
            Glide.with(holder.itemView.context)
                .load(use.first.toUri())
                .listener(CustomGlideListener(myTrace,url,true))
                .error(
                    Glide.with(holder.itemView.context)
                        .load(use.second.toUri())
                        .listener(CustomGlideListener(null,url,false))
                        .apply(
                            RequestOptions()
                                .placeholder(R.drawable.ic_baseline_add_photo_alternate_24)
                                .error(R.drawable.ic_baseline_warning_24)
                        )
                )
                .centerInside()
                .into(holder.contentPicture)
        }

        MainScope().launch {
            var url = "users/${currentItem.owner}/profile.jpeg"
            var use = FirestoreService.getImage(url,currentItem.owner?:"")
            var myTrace = Firebase.performance.newTrace("get_image")
            myTrace.start()
            Glide.with(holder.itemView.context)
                .load(use.first.toUri())
                .listener(CustomGlideListener(myTrace,url,true))
                .error(
                    Glide.with(holder.itemView.context)
                        .load(use.second.toUri())
                        .listener(CustomGlideListener(null,url,false))
                        .apply(
                            RequestOptions()
                                .placeholder(R.drawable.ic_baseline_add_photo_alternate_24)
                                .error(R.drawable.ic_baseline_warning_24)
                        )
                )
                .circleCrop()
                .into(holder.postPicture)
        }
    }

    override fun getItemCount(): Int {
        return postList.size
    }


    override fun onViewRecycled(holder: MyViewHolder) {
        super.onViewRecycled(holder)
        Glide.with(holder.itemView.context).
                clear(holder.postPicture)
        Glide.with(holder.itemView.context).
            clear(holder.contentPicture)
    }

    class MyViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val postPicture: ImageView = itemView.findViewById(R.id.postPicture)
        val postName: TextView = itemView.findViewById(R.id.postName)
        val postDate : TextView = itemView.findViewById(R.id.postDate)
        val postContent : TextView = itemView.findViewById(R.id.postContent)
        val contentPicture : ImageView = itemView.findViewById(R.id.contentPicture)
    }
}