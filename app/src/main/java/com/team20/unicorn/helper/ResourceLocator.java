package com.team20.unicorn.helper;

public class ResourceLocator {

    private String key;

    private String source;

    public ResourceLocator(String source, String key){
        this.key = key;
        this.source = source;
    }

    @Override
    public String toString(){
        return this.key;
    }

    public String getKey() {
        return key;
    }

    public String getSource() {
        return source;
    }
}
