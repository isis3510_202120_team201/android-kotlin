package com.team20.unicorn.helper

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.team20.unicorn.R
import com.team20.unicorn.model.StartupPage
import com.team20.unicorn.viewModel.PagesViewModel

class PagesAdapter (private val weeklies: List<StartupPage>, private val pagesView: PagesViewModel, private val redirect: Boolean)  : RecyclerView.Adapter<PagesAdapter.ViewHolder>() {

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val pictureIV=itemView.findViewById<ImageView>(R.id.weeklyPicture)
        val nameTextView = itemView.findViewById<TextView>(R.id.weeklyName)
        val countryTextView = itemView.findViewById<TextView>(R.id.weeklyCountry)
        val descriptionTextView = itemView.findViewById<TextView>(R.id.weeklyDescription)
        val preferenceTextView = itemView.findViewById<TextView>(R.id.weeklyPreference)
        val cardPage=itemView.findViewById<androidx.cardview.widget.CardView>(R.id.cardPage)
    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagesAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val weeklyView = inflater.inflate(R.layout.weekly_item, parent, false)
        // Return a new holder instance
        return ViewHolder(weeklyView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: PagesAdapter.ViewHolder, position: Int) {
        // Get the data model based on position
        val weekly: StartupPage = weeklies.get(position)

        // Set item views based on your views and data model
        val nameTV = viewHolder.nameTextView
        nameTV.setText(weekly.name)
        val countryTV = viewHolder.countryTextView
        countryTV.setText(weekly.country)
        val descriptionTV = viewHolder.descriptionTextView
        descriptionTV.setText(weekly.description)
        val preferenceTV = viewHolder.preferenceTextView
        preferenceTV.setText(weekly.preferredFinancial)
        val cardPage = viewHolder.cardPage

        val url = "posts/${weekly.id}/profile.jpeg"
        var myTrace = Firebase.performance.newTrace("get_image")
        myTrace.start()
        Glide.with(viewHolder.itemView.context)
            .load(weekly.profilePicUrl)
            .listener(CustomGlideListener(myTrace,url,true))
            .error(
                RequestOptions()
                    .placeholder(R.drawable.ic_baseline_add_photo_alternate_24)
                    .error(R.drawable.ic_baseline_warning_24)
            )
            .circleCrop()
            .into(viewHolder.pictureIV)


        if(redirect) {
            viewHolder.cardPage.setOnClickListener {
                pagesView.updateWeekly(weekly.id)

            }
            viewHolder.countryTextView.setOnClickListener {
                pagesView.updateWeekly(weekly.id)
            }
            viewHolder.descriptionTextView.setOnClickListener {
                pagesView.updateWeekly(weekly.id)
            }
            viewHolder.nameTextView.setOnClickListener {
                pagesView.updateWeekly(weekly.id)
            }
            viewHolder.pictureIV.setOnClickListener {
                pagesView.updateWeekly(weekly.id)
            }
            viewHolder.preferenceTextView.setOnClickListener {
                pagesView.updateWeekly(weekly.id)
            }
        }
    }

    override fun onViewRecycled(holder: PagesAdapter.ViewHolder) {
        super.onViewRecycled(holder)
        Glide.with(holder.itemView.context).
        clear(holder.pictureIV)
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return weeklies.size
    }
}