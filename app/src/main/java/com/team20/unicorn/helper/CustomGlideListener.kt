package com.team20.unicorn.helper

import android.graphics.drawable.Drawable
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.firebase.perf.metrics.Trace
import com.team20.unicorn.repository.cache.CacheRoutes

class CustomGlideListener(val myTrace: Trace?, val url: String,
                          val keep: Boolean) : RequestListener<Drawable> {
    override fun onLoadFailed(
        e: GlideException?,
        model: Any?,
        target: Target<Drawable>?,
        isFirstResource: Boolean
    ): Boolean {
        try {
            myTrace?.stop()
        }catch (e: Exception){
        }
        if(!keep){
            CacheRoutes.deleteKey(url)
        }
        return false
    }

    override fun onResourceReady(
        resource: Drawable?,
        model: Any?,
        target: Target<Drawable>?,
        dataSource: DataSource?,
        isFirstResource: Boolean
    ): Boolean {
        try {
            myTrace?.stop()
        }catch (e: Exception){
        }
        return false
    }
}