package com.team20.unicorn.helper

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.team20.unicorn.R
import com.team20.unicorn.model.User
import com.team20.unicorn.repository.database.FirestoreService
import com.team20.unicorn.viewModel.SearchUserViewModel
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class SearchAdapter(private val postList: ArrayList<User>, private val searchView: SearchUserViewModel): RecyclerView.Adapter<SearchAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchAdapter.MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.search_item,parent,false)
        return SearchAdapter.MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = postList[position]

        holder.searchFirstName.text = currentItem.firstName
        holder.searchLastName.text = currentItem.lastName
        holder.searchLikes.text = currentItem.interests?.joinToString() ?: ""

        holder.searchPicture.setOnClickListener {
            searchView.goToProfile(currentItem.id?:"")
        }

        MainScope().launch {
            var url = "users/${currentItem.id}/profile.jpeg"
            var use = FirestoreService.getImage(url,currentItem.id?:"")
            Log.i("IMAGES","url is $url\nuse.first = ${use.first}\nuse.second = ${use.second}")
            var myTrace = Firebase.performance.newTrace("get_image")
            myTrace.start()
            Glide.with(holder.itemView.context)
                .load(use.first.toUri())
                .listener(CustomGlideListener(myTrace,url,true))
                .error(
                    Glide.with(holder.itemView.context)
                        .load(use.second.toUri())
                        .listener(CustomGlideListener(null,url,false))
                        .apply(
                            RequestOptions()
                                .placeholder(R.drawable.ic_baseline_add_photo_alternate_24)
                                .error(R.drawable.ic_baseline_warning_24)
                        )
                )
                .circleCrop()
                .into(holder.searchPicture)
        }

    }

    override fun onViewRecycled(holder: MyViewHolder) {
        super.onViewRecycled(holder)
        Glide.with(holder.itemView.context).
        clear(holder.searchPicture)
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    class MyViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val searchPicture: ImageView = itemView.findViewById(R.id.thumbnail)
        val searchFirstName: TextView = itemView.findViewById(R.id.firstName)
        val searchLastName: TextView = itemView.findViewById(R.id.LastNameSearch)
        val searchLikes: TextView = itemView.findViewById(R.id.Likes)
    }
}
