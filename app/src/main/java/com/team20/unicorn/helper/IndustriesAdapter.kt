package com.team20.unicorn.helper

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.team20.unicorn.R
import com.team20.unicorn.model.Industry

class IndustriesAdapter(private val industries: ArrayList<Industry>)  : RecyclerView.Adapter<IndustriesAdapter.ViewHolder>() {

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val nameTextView = itemView.findViewById<TextView>(R.id.trendsTxt)
        val fundingTextView = itemView.findViewById<TextView>(R.id.fundingTxt)
        val startupsTextView = itemView.findViewById<TextView>(R.id.startupTxt)
    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IndustriesAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val industryView = inflater.inflate(R.layout.industry_item, parent, false)
        // Return a new holder instance
        return ViewHolder(industryView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: IndustriesAdapter.ViewHolder, position: Int) {
        // Get the data model based on position
        val industry: Industry = industries.get(position)
        // Set item views based on your views and data model
        val nameTV = viewHolder.nameTextView
        nameTV.setText(industry.name)
        val fundingTV = viewHolder.fundingTextView
        fundingTV.setText("$"+industry.funding.toString())
        val startupsTV = viewHolder.startupsTextView
        startupsTV.setText(industry.startups.toString())
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return industries.size
    }
}