package com.team20.unicorn.helper

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.team20.unicorn.R

class InterestsAdapter(private val interests: MutableList<String>): RecyclerView.Adapter<InterestsAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val textInt = itemView.findViewById<TextView>(R.id.interestProfile)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InterestsAdapter.ViewHolder  {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val contactView = inflater.inflate(R.layout.interest_item, parent, false)
        // Return a new holder instance
        return ViewHolder(contactView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: InterestsAdapter.ViewHolder, position: Int) {
        // Get the data model based on position
        val interest: String = interests.get(position)
        // Set item views based on your views and data model
        val textView = viewHolder.textInt
        textView.setText(interest)

    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return interests.size
    }

}