package com.team20.unicorn

import android.content.DialogInterface
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.team20.unicorn.databinding.FragmentTrendsBinding
import com.team20.unicorn.helper.ConnectionHelper
import com.team20.unicorn.helper.TrendsAdapter
import com.team20.unicorn.viewModel.TrendsViewModel

class TrendsFragment : Fragment(),  AdapterView.OnItemSelectedListener {

    private lateinit var viewModel: TrendsViewModel
    private  lateinit var trendsRecyclerView: RecyclerView
    private  lateinit var spinnerCoutries: Spinner
    private lateinit var binding : FragmentTrendsBinding
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(TrendsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val activity = activity as MainActivity
        binding = DataBindingUtil.inflate<FragmentTrendsBinding>(inflater,R.layout.fragment_trends,container,false)
        if(connectionHelper.checkForInternet(requireContext())) {
            binding.progressBarTrends.isVisible = true
            viewModel.getCountries(requireContext())
            trendsRecyclerView = binding.trendsList
            spinnerCoutries = binding.spinnerCountries
            trendsRecyclerView.layoutManager = LinearLayoutManager(activity)
            viewModel.trendsArrayList.observe(viewLifecycleOwner, Observer { newArrayList ->
                binding.progressBarTrends.isVisible=false
                trendsRecyclerView.adapter = TrendsAdapter(newArrayList)
            })

            viewModel.countryArrayList.observe(viewLifecycleOwner, Observer { newArrayList ->
                val adapter: ArrayAdapter<String> = ArrayAdapter<String>(activity,
                    android.R.layout.simple_spinner_item, newArrayList)
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerCoutries.setAdapter(adapter)
                spinnerCoutries.onItemSelectedListener = this
            })
        }
        else
        {
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.apply {
                setTitle("Internet connection")
                setMessage("To use this feature you need internet connection")
                setNeutralButton("Ok",
                    DialogInterface.OnClickListener { dialog, id ->
                        binding.txtNoConnTrends.isVisible = true
                        binding.noConnection.isVisible=true
                    })
            }.create().show()
        }

        return binding.root
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using
        binding.progressBarTrends.isVisible=false
        viewModel.getPerCountry(parent.getItemAtPosition(pos).toString(), requireContext())
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }


}