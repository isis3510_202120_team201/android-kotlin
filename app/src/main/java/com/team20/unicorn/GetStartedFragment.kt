package com.team20.unicorn

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.team20.unicorn.databinding.*
import com.team20.unicorn.helper.ConnectionHelper
import com.team20.unicorn.viewModel.GetStartedViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [GetStartedFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GetStartedFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var viewModel: GetStartedViewModel
    private lateinit var buttonSignUp: Button
    private lateinit var inputFirstName: EditText
    private lateinit var inputLastName: EditText
    private lateinit var inputEmail: EditText
    private lateinit var password: EditText
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }
    private lateinit var check: CheckBox
        private lateinit var phoneNumber: EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentGetStartedBinding>(inflater,R.layout.fragment_get_started,container,false)
        viewModel = ViewModelProvider(this).get(GetStartedViewModel::class.java)

        val activity = activity as MainActivity
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null) {
            viewModel.userId = sharedPref.getString("userId","").toString();
        }
        viewModel.successAuth.observe(viewLifecycleOwner, Observer { newSucess ->
            handleSuccessAuth(newSucess)
        })

        viewModel.successCreate.observe(viewLifecycleOwner, Observer { newSucess ->
            handleSuccessCreated(newSucess)
        })
        binding.getStartedViewModel = viewModel

        binding.lifecycleOwner = viewLifecycleOwner

        binding.backButton.setOnClickListener{ view: View ->
            view.findNavController().navigate(R.id.action_getStartedFragment_to_welcomeFragment2)
        }

        buttonSignUp = binding.GetStartedButton
        inputFirstName = binding.inputFirstName
        inputLastName = binding.inputLastName
        inputEmail = binding.inputEmail
        password = binding.inputPassword

        inputFirstName.addTextChangedListener(textWatcher)
        inputLastName.addTextChangedListener(textWatcher)
        inputEmail.addTextChangedListener(textWatcher)
        password.addTextChangedListener(textWatcher)

        check = binding.checkTwoFactor
        phoneNumber = binding.inputPhoneNumber

        viewModel.dialogMessage.observe(viewLifecycleOwner, Observer { message ->
            showDialog("Failed to create user", message)
        })

        buttonSignUp.isEnabled = false
        buttonSignUp.setOnClickListener { view: View ->
            createUser()
        }



        return binding.root
    }


 private fun createUser ()
 {
     if(connectionHelper.checkForInternet(requireContext())) {
         val email =inputEmail.text.toString()
         if(isValidEmail(email)) {
             viewModel.createUser(
                 inputFirstName.text.toString(),
                 inputLastName.text.toString(),
                 email,
                 password.text.toString(),
                 phoneNumber.text.toString(),
                 check.isChecked
             )
         }
         else{
             showDialog("Failed to create user", "The email doesn't have a valid format")
         }
     }
     else
     {
         showDialog("Internet connection", "To create an user in Unicorn App you need internet connection")
     }
 }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            buttonSignUp.isEnabled = !(inputFirstName.text.toString().length==0 ||
                    inputLastName.text.toString().length==0 ||
                    inputEmail.text.toString().length==0 ||
                    password.text.toString().length<6)

        }
    }
    private fun handleSuccessAuth(newSuccess: Boolean){
        if(newSuccess){
            val activity = activity as MainActivity
            val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
            if(sharedPref != null) {
                with (sharedPref.edit()) {
                    putString("userId", viewModel.userId)
                    apply()
                }
            }

        }
        else{
            Toast.makeText(activity as Activity, "Authentication failed.",
                Toast.LENGTH_SHORT).show()
        }
    }
    private fun handleSuccessCreated(newSuccess: Boolean){
        if(newSuccess){
            val activity = activity as MainActivity

            Toast.makeText(activity as Activity, "Creating user succeeded", Toast.LENGTH_SHORT).show()
            if(!viewModel.toTwoFactor)
                requireView().findNavController().navigate(R.id.action_getStartedFragment_to_chooseRoleFragment)
            else
                requireView().findNavController().navigate(GetStartedFragmentDirections.actionGetStartedFragmentToTwoFactorFragment(phoneNumber.text.toString(),true))
        }
        else{
            Toast.makeText(activity as Activity, "Failed creating user in database",
                Toast.LENGTH_SHORT).show()
        }
    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }


    fun showDialog (title:String, message: String)
    {
        val alertDialog = AlertDialog.Builder(requireContext())
        alertDialog.apply {
            setTitle(title)
            setMessage(message)
            setNeutralButton("Ok",
                { dialog, id ->

                })
        }.create().show()
    }

}