package com.team20.unicorn

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.team20.unicorn.databinding.FragmentStartupsBinding
import com.team20.unicorn.helper.ConnectionHelper
import com.team20.unicorn.helper.PagesAdapter
import com.team20.unicorn.viewModel.PagesViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [StartupsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StartupsFragment : Fragment() {
    private lateinit var viewModel: PagesViewModel
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }
    private  lateinit var weekliesRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { viewModel = ViewModelProvider(this).get(PagesViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(PagesViewModel::class.java)
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentStartupsBinding>(inflater,R.layout.fragment_startups,container,false)
        val activity = activity as MainActivity
        var id =""
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null && id == "") {
            id = sharedPref.getString("userId","").toString()
        }

            binding.progressBarStartupsProfile.isVisible=true
            viewModel.getPagesById(id)
            weekliesRecyclerView = binding.startupsProfileList
            weekliesRecyclerView.layoutManager = LinearLayoutManager(activity)
            viewModel.pagesArrayList.observe(viewLifecycleOwner, Observer { newArrayList ->
                binding.progressBarStartupsProfile.isVisible = false
                if(newArrayList.isEmpty())
                {
                    binding.noPagesStartups.isVisible=true
                }
                else {
                    weekliesRecyclerView.adapter = PagesAdapter(newArrayList,viewModel,false)
                }
            })


        if (!connectionHelper.checkForInternet(requireContext())) {
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.apply {
                setTitle("Internet connection")
                setMessage("You don't have internet connection. We are showing cached info in your phone.")
                setNeutralButton("Ok",
                    DialogInterface.OnClickListener { dialog, id ->
                    })
            }.create().show()

        }

        return binding.root
    }


}