package com.team20.unicorn

import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.team20.unicorn.databinding.FragmentChooseRoleBinding
import com.team20.unicorn.viewModel.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ChooseRoleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChooseRoleFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var viewModel: ChooseRoleViewModel

    private lateinit var buttonBack: ImageButton
    private lateinit var buttonEntrepeneur: Button
    private lateinit var buttonInvestor: Button
    private lateinit var buttonNext: Button
    private lateinit var userType: String
    private lateinit var viewModelFactory: ChooseRoleViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentChooseRoleBinding>(inflater,R.layout.fragment_choose_role,container,false)

        val activity = activity as MainActivity
        var id = ""

        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        if(sharedPref != null) {
            id = sharedPref.getString("userId","").toString()
        }

        viewModelFactory = ChooseRoleViewModelFactory(id)

        viewModel = ViewModelProvider(this,viewModelFactory)
            .get(ChooseRoleViewModel::class.java)

        viewModel.success.observe(viewLifecycleOwner, Observer { newSucess ->
            handleSuccess(newSucess)
        })

        binding.lifecycleOwner = viewLifecycleOwner

        buttonBack = binding.backButton
        buttonEntrepeneur = binding.bttnEntrepeneur
        buttonInvestor = binding.bttnInvestor
        buttonNext = binding.bttnNext

        buttonNext.isEnabled = false

        userType = ""

        buttonInvestor.setOnClickListener {
            onClick(buttonInvestor)
        }
        buttonEntrepeneur.setOnClickListener {
            onClick(buttonEntrepeneur)
        }
        buttonNext.setOnClickListener { view : View->
            viewModel.addRole(userType)
        }
        buttonBack.setOnClickListener{ view: View ->
            view.findNavController().navigate(R.id.action_chooseRoleFragment_to_getStartedFragment)
        }
        return binding.root
    }
    fun onClick(v: View){
        val animatorFadeoutInvestor = ObjectAnimator.ofFloat(buttonInvestor, View.ALPHA, 0.5f)
        val animatorFadeoutEntrepeneur = ObjectAnimator.ofFloat(buttonEntrepeneur, View.ALPHA, 0.5f)
        val animatorFadeinInvestor = ObjectAnimator.ofFloat(buttonInvestor, View.ALPHA, 1f)
        val animatorFadeinEntrepeneur = ObjectAnimator.ofFloat(buttonEntrepeneur, View.ALPHA, 1f)
        when(v.id) {
            buttonEntrepeneur.id -> {
                userType = "Entrepeneur"
                animatorFadeinEntrepeneur.start()
                animatorFadeoutInvestor.start()
                buttonNext.isEnabled = true

            }
            buttonInvestor.id -> {
                userType = "Investor"
                animatorFadeinInvestor.start()
                animatorFadeoutEntrepeneur.start()
                buttonNext.isEnabled = true
            }

        }
    }
    fun handleSuccess(newSuccess: Boolean){
        if(newSuccess) {
            Toast.makeText(activity as Activity, "Success selecting role", Toast.LENGTH_SHORT)
                .show()
            requireView().findNavController()
                .navigate(R.id.action_chooseRoleFragment_to_preferencesFragment)
        }
        else{
            Toast.makeText(activity as Activity, "Failed selecting role", Toast.LENGTH_LONG).show()
        }
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ChooseRoleFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ChooseRoleFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}