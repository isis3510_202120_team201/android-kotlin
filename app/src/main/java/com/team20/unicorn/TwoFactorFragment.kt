package com.team20.unicorn

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.team20.unicorn.databinding.FragmentTwoFactorBinding
import com.team20.unicorn.viewModel.TwoFactorViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TwoFactorFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TwoFactorFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var viewModel: TwoFactorViewModel
    private lateinit var textCode: EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentTwoFactorBinding>(inflater,R.layout.fragment_two_factor,container,false)

        val args = TwoFactorFragmentArgs.fromBundle(requireArguments())

        viewModel = ViewModelProvider(this).get(TwoFactorViewModel::class.java)

        activity?.let { viewModel.triggerAuthentication(args.phoneNumber, it) }

        viewModel.success.observe(viewLifecycleOwner, Observer {
            if(args.chooseFragment)
                requireView().findNavController().navigate(R.id.action_twoFactorFragment_to_chooseRoleFragment)
            else
                requireView().findNavController().navigate(R.id.action_twoFactorFragment_to_mainFragment)
        })

        textCode = binding.inputPhoneNumber

        binding.authConfirmation.setOnClickListener{
            activity?.let { it1 -> viewModel.authenticate(textCode.text.toString(), it1) }
        }


        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TwoFactorFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TwoFactorFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}