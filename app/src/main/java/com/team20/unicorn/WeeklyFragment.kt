package com.team20.unicorn

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.team20.unicorn.databinding.FragmentWeeklyBinding
import com.team20.unicorn.helper.ConnectionHelper
import com.team20.unicorn.helper.PagesAdapter
import com.team20.unicorn.viewModel.PagesViewModel

/**
 * A simple [Fragment] subclass.
 * Use the [WeeklyFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WeeklyFragment : Fragment() {

    private lateinit var viewModel: PagesViewModel
    private val connectionHelper: ConnectionHelper by lazy { ConnectionHelper() }
    private  lateinit var weekliesRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PagesViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentWeeklyBinding>(inflater,R.layout.fragment_weekly,container,false)
        viewModel.toGoWeekly.observe(viewLifecycleOwner, Observer {
            var see = parentFragment
            when(see){
                is NavHostFragment -> see = see.parentFragment
                else -> { }
            }
            Log.i("GALO", see.toString())
            when(see){
                is WeeklyHotFragment -> see = see.parentFragment
                else -> { }
            }
            when(see){
                is NavHostFragment -> see = see.parentFragment
                else -> { }
            }
            when(see){
                is GenericDashboardFragment -> see.swapToPagesProfile(it)
                else -> {}
            }
        })
            binding.progressBarWeekly.isVisible=true
            viewModel.getPagesWeekly()
            weekliesRecyclerView = binding.weeklyList
            weekliesRecyclerView.layoutManager = LinearLayoutManager(activity)
            viewModel.pagesArrayList.observe(viewLifecycleOwner, Observer { newArrayList ->
                binding.progressBarWeekly.isVisible = false
                weekliesRecyclerView.adapter = PagesAdapter(newArrayList,viewModel,true)
            })

        if (!connectionHelper.checkForInternet(requireContext())) {
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.apply {
                setTitle("Internet connection")
                setMessage("You don't have internet connection. We are showing cached info in your phone.")
                setNeutralButton("Ok",
                    DialogInterface.OnClickListener { dialog, id ->
                    })
            }.create().show()
        }

        return binding.root
    }


}